package com.gex.emp.api.controller;

import com.gex.emp.persistence.entity.Position;
import com.gex.emp.persistence.repository.PositionRepository;
import com.gex.sec.api.security.SecurityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/position")
@RequiredArgsConstructor
public class PositionController {

    public static final String POSITION = "position";

    private final SecurityUtil security;
    private final PositionRepository positionRepository;

    @GetMapping
    public ResponseEntity<List<Position>> getPositions() {
        return security.authorized(token -> ResponseEntity.ok(positionRepository.findAll()));
    }

    @GetMapping("{id}")
    public ResponseEntity<Position> getPosition(@PathVariable String id) {
        return security.authorized(token -> positionRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> security.notFound(POSITION)));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deletePosition(@PathVariable String id) {
        return security.admin(token -> positionRepository.findById(id)
                .map(position -> {
                    positionRepository.deleteById(id);
                    return ResponseEntity.ok().<Void>build();
                }).orElseGet(() -> security.notFound(POSITION)));
    }

    @PostMapping
    public ResponseEntity<Position> createPosition(@RequestBody @Valid Position.CreationTransfer transfer) {
        return security.admin(token -> ResponseEntity.ok(positionRepository.save(transfer.merge(new Position()))));
    }

    @PutMapping
    public ResponseEntity<Position> modifyPosition(@RequestBody @Valid Position.ModificationTransfer transfer) {
        return security.admin(token -> positionRepository.findById(transfer.getId())
                .map(position -> ResponseEntity.ok(positionRepository.save(transfer.merge(new Position()))))
                .orElseGet(() -> security.notFound(POSITION)));
    }

}
