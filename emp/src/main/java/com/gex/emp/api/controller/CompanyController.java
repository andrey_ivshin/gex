package com.gex.emp.api.controller;

import com.gex.emp.persistence.entity.Company;
import com.gex.emp.persistence.repository.CompanyRepository;
import com.gex.sec.api.security.SecurityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/company")
@RequiredArgsConstructor
public class CompanyController {

    public static final String COMPANY = "company";

    private final SecurityUtil security;
    private final CompanyRepository companyRepository;

    @GetMapping
    public ResponseEntity<List<Company>> getCompanies() {
        return security.authorized(token -> ResponseEntity.ok(companyRepository.findAll()));
    }

    @GetMapping("{id}")
    public ResponseEntity<Company> getCompany(@PathVariable String id) {
        return security.authorized(token -> companyRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> security.notFound(COMPANY)));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteCompany(@PathVariable String id) {
        return security.admin(token -> companyRepository.findById(id)
                .map(company -> {
                    companyRepository.deleteById(id);
                    return ResponseEntity.ok().<Void>build();
                }).orElseGet(() -> security.notFound(COMPANY)));
    }

    @PostMapping
    public ResponseEntity<Company> createCompany(@RequestBody @Valid Company.CreationTransfer transfer) {
        return security.admin(token -> ResponseEntity.ok(companyRepository.save(transfer.merge(new Company()))));
    }

    @PutMapping
    public ResponseEntity<Company> modifyCompany(@RequestBody @Valid Company.ModificationTransfer transfer) {
        return security.admin(token -> companyRepository.findById(transfer.getId())
                .map(company -> ResponseEntity.ok(companyRepository.save(transfer.merge(new Company()))))
                .orElseGet(() -> security.notFound(COMPANY)));
    }

}
