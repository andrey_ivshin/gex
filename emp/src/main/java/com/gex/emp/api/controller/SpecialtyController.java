package com.gex.emp.api.controller;

import com.gex.emp.persistence.entity.Specialty;
import com.gex.emp.persistence.repository.SpecialtyRepository;
import com.gex.sec.api.security.SecurityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/specialty")
@RequiredArgsConstructor
public class SpecialtyController {

    public static final String SPECIALTY = "specialty";

    private final SecurityUtil security;
    private final SpecialtyRepository specialtyRepository;

    @GetMapping
    public ResponseEntity<List<Specialty>> getSpecialties() {
        return security.authorized(token -> ResponseEntity.ok(specialtyRepository.findAll()));
    }

    @GetMapping("{id}")
    public ResponseEntity<Specialty> getSpecialty(@PathVariable String id) {
        return security.authorized(token -> specialtyRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> security.notFound(SPECIALTY)));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteSpecialty(@PathVariable String id) {
        return security.admin(token -> specialtyRepository.findById(id)
                .map(specialty -> {
                    specialtyRepository.deleteById(id);
                    return ResponseEntity.ok().<Void>build();
                }).orElseGet(() -> security.notFound(SPECIALTY)));
    }

    @PostMapping
    public ResponseEntity<Specialty> createSpecialty(@RequestBody @Valid Specialty.CreationTransfer transfer) {
        return security.admin(token -> ResponseEntity.ok(specialtyRepository.save(transfer.merge(new Specialty()))));
    }

    @PutMapping
    public ResponseEntity<Specialty> modifySpecialty(@RequestBody @Valid Specialty.ModificationTransfer transfer) {
        return security.admin(token -> specialtyRepository.findById(transfer.getId())
                .map(specialty -> ResponseEntity.ok(specialtyRepository.save(transfer.merge(new Specialty()))))
                .orElseGet(() -> security.notFound(SPECIALTY)));
    }

}
