package com.gex.emp.api.controller;

import com.gex.emp.domain.model.SearchCriteria;
import com.gex.emp.persistence.entity.*;
import com.gex.emp.persistence.repository.*;
import com.gex.sec.api.security.SecurityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.gex.emp.api.controller.CompanyController.COMPANY;
import static com.gex.emp.api.controller.InstitutionController.INSTITUTION;
import static com.gex.emp.api.controller.LanguageController.LANGUAGE;
import static com.gex.emp.api.controller.PositionController.POSITION;
import static com.gex.emp.api.controller.SkillController.SKILL;
import static com.gex.emp.api.controller.SpecialtyController.SPECIALTY;
import static com.gex.sec.api.security.Executor.ExecutorChain.exec;
import static com.gex.sec.api.security.SecurityUtil.ALREADY_EXIST;

@RestController
@RequestMapping("api/profile")
@RequiredArgsConstructor
public class ProfileController {

    public static final String PROFILE = "profile";
    public static final String CONTACT = "contact";
    public static final String EXPERIENCE = "experience";
    public static final String EDUCATION = "education";
    public static final String ADDITIONAL = "additional";
    public static final String POINT = "point";

    private final SecurityUtil security;
    private final ProfileRepository profileRepository;
    private final PositionRepository positionRepository;
    private final SkillRepository skillRepository;
    private final LanguageRepository languageRepository;
    private final CompanyRepository companyRepository;
    private final InstitutionRepository institutionRepository;
    private final SpecialtyRepository specialtyRepository;
    private final ProfileSearchRepository profileSearchRepository;

    @GetMapping
    public ResponseEntity<List<Profile>> getProfiles() {
        return security.admin(token -> ResponseEntity.ok(profileRepository.findAll()));
    }

    @PostMapping("search")
    public ResponseEntity<List<Profile>> getProfiles(@RequestBody SearchCriteria searchCriteria) {
        return security.admin(token -> ResponseEntity.ok(profileSearchRepository.find(searchCriteria)));
    }

    @GetMapping("{id}")
    public ResponseEntity<Profile> getProfile(@PathVariable String id) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteProfile(@PathVariable String id) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> {
                    profileRepository.deleteById(id);
                    return ResponseEntity.ok().<Void>build();
                }).orElseGet(() -> security.notFound(PROFILE)));
    }

    @PostMapping
    public ResponseEntity<Profile> createProfile(@RequestBody @Valid Profile.Transfer transfer) {
        return security.checkId(transfer.getId(), token -> profileRepository.findById(transfer.getId())
                .map(profile -> security.<Profile>error(HttpStatus.BAD_REQUEST, ALREADY_EXIST))
                .orElseGet(() -> ResponseEntity.ok(profileRepository.save(transfer.merge(new Profile())))));
    }

    @PutMapping
    public ResponseEntity<Profile> modifyProfile(@RequestBody @Valid Profile.Transfer transfer) {
        return security.checkId(transfer.getId(), token -> profileRepository.findById(transfer.getId())
                .map(profile -> ResponseEntity.ok(profileRepository.save(transfer.merge(profile))))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/position/{iid}")
    public ResponseEntity<Profile> setPosition(@PathVariable String id, @PathVariable String iid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> positionRepository.findById(iid)
                        .map(position -> {
                            profile.setPosition(position);
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(POSITION)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/position")
    public ResponseEntity<Profile> unsetPosition(@PathVariable String id) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> {
                    profile.setPosition(null);
                    return ResponseEntity.ok(profileRepository.save(profile));
                }).orElseGet(() -> security.notFound(PROFILE)));
    }

    @PostMapping("{id}/contact")
    public ResponseEntity<Profile> addContact(@PathVariable String id, @RequestBody @Valid Contact.CreationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> {
                    profile.getContacts().add(transfer.merge(new Contact()));
                    return ResponseEntity.ok(profileRepository.save(profile));
                }).orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/contact")
    public ResponseEntity<Profile> modifyContact(@PathVariable String id, @RequestBody @Valid Contact.ModificationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getContacts().stream().filter(c -> c.getId().equals(transfer.getId())).findAny()
                        .map(contact -> {
                            transfer.merge(contact);
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(CONTACT)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/contact/{iid}")
    public ResponseEntity<Profile> deleteContact(@PathVariable String id, @PathVariable String iid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getContacts().stream().filter(c -> c.getId().equals(iid)).findAny()
                        .map(contact -> {
                            profile.getContacts().remove(contact);
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(CONTACT)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/skill/{iid}")
    public ResponseEntity<Profile> setSkill(@PathVariable String id, @PathVariable String iid, @RequestBody Skill.LevelTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> skillRepository.findById(iid)
                        .map(skill -> {
                            profile.getSkills().put(skill, transfer.getLevel());
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(SKILL)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/skill/{iid}")
    public ResponseEntity<Profile> unsetSkill(@PathVariable String id, @PathVariable String iid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> skillRepository.findById(iid)
                        .map(skill -> profile.getSkills().containsKey(skill)
                                ? exec(() -> profile.getSkills().remove(skill))
                                .andReturn(() -> ResponseEntity.ok(profileRepository.save(profile)))
                                : security.<Profile>notFoundIn(PROFILE, SKILL))
                        .orElseGet(() -> security.notFound(SKILL)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/language/{iid}")
    public ResponseEntity<Profile> setLanguage(@PathVariable String id, @PathVariable String iid, @RequestBody Language.LanguageLevelTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> languageRepository.findById(iid)
                        .map(language -> {
                            profile.getLanguages().put(language, transfer.getLevel());
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(LANGUAGE)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/language/{iid}")
    public ResponseEntity<Profile> deleteLanguage(@PathVariable String id, @PathVariable String iid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> languageRepository.findById(iid)
                        .map(language -> profile.getLanguages().containsKey(language)
                                ? exec(() -> profile.getLanguages().remove(language))
                                .andReturn(() -> ResponseEntity.ok(profileRepository.save(profile)))
                                : security.<Profile>notFoundIn(PROFILE, LANGUAGE))
                        .orElseGet(() -> security.notFound(SKILL)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }


    @PostMapping("{id}/experience")
    public ResponseEntity<Profile> addExperience(@PathVariable String id, @RequestBody @Valid Experience.CreationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> {
                    profile.getExperience().add(transfer.merge(new Experience()));
                    return ResponseEntity.ok(profileRepository.save(profile));
                }).orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/experience")
    public ResponseEntity<Profile> modifyExperience(@PathVariable String id, @RequestBody @Valid Experience.ModificationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getExperience().stream().filter(e -> e.getId().equals(transfer.getId())).findAny()
                        .map(experience -> {
                            transfer.merge(experience);
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(EXPERIENCE)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/experience/{iid}")
    public ResponseEntity<Profile> deleteExperience(@PathVariable String id, @PathVariable String iid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getExperience().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(experience -> {
                            profile.getExperience().remove(experience);
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(EXPERIENCE)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/experience/{iid}/company/{iiid}")
    public ResponseEntity<Profile> setExperienceCompany(@PathVariable String id, @PathVariable String iid, @PathVariable String iiid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getExperience().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(experience -> companyRepository.findById(iiid)
                                .map(company -> {
                                    experience.setCompany(company);
                                    return ResponseEntity.ok(profileRepository.save(profile));
                                }).orElseGet(() -> security.notFound(COMPANY)))
                        .orElseGet(() -> security.notFound(EXPERIENCE)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/experience/{iid}/company")
    public ResponseEntity<Profile> unsetExperienceCompany(@PathVariable String id, @PathVariable String iid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getExperience().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(experience -> {
                            experience.setCompany(null);
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(EXPERIENCE)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/experience/{iid}/position/{iiid}")
    public ResponseEntity<Profile> setExperiencePosition(@PathVariable String id, @PathVariable String iid, @PathVariable String iiid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getExperience().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(experience -> positionRepository.findById(iiid)
                                .map(position -> {
                                    experience.setPosition(position);
                                    return ResponseEntity.ok(profileRepository.save(profile));
                                }).orElseGet(() -> security.notFound(POSITION)))
                        .orElseGet(() -> security.notFound(EXPERIENCE)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/experience/{iid}/position")
    public ResponseEntity<Profile> unsetExperiencePosition(@PathVariable String id, @PathVariable String iid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getExperience().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(experience -> {
                            experience.setPosition(null);
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(EXPERIENCE)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @PostMapping("{id}/experience/{iid}/point")
    public ResponseEntity<Profile> addExperiencePoint(@PathVariable String id, @PathVariable String
            iid, @RequestBody @Valid Point.CreationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getExperience().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(experience -> {
                            experience.getPoints().add(transfer.merge(new Point()));
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(EXPERIENCE))
                ).orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/experience/{iid}/point")
    public ResponseEntity<Profile> modifyExperiencePoint(@PathVariable String id, @PathVariable String
            iid, @RequestBody @Valid Point.ModificationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getExperience().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(experience -> experience.getPoints().stream().filter(p -> p.getId().equals(transfer.getId())).findAny()
                                .map(point -> {
                                    transfer.merge(point);
                                    return ResponseEntity.ok(profileRepository.save(profile));
                                }).orElseGet(() -> security.notFound(POINT)))
                        .orElseGet(() -> security.notFound(EXPERIENCE)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/experience/{iid}/point/{iiid}")
    public ResponseEntity<Profile> deleteExperiencePoint(@PathVariable String id, @PathVariable String
            iid, @PathVariable String iiid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getExperience().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(experience -> experience.getPoints().stream().filter(p -> p.getId().equals(iiid)).findAny()
                                .map(point -> {
                                    experience.getPoints().remove(point);
                                    return ResponseEntity.ok(profileRepository.save(profile));
                                }).orElseGet(() -> security.notFound(POINT)))
                        .orElseGet(() -> security.notFound(EXPERIENCE)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }


    @PostMapping("{id}/education")
    public ResponseEntity<Profile> addEducation(@PathVariable String id, @RequestBody @Valid Education.CreationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> {
                    profile.getEducation().add(transfer.merge(new Education()));
                    return ResponseEntity.ok(profileRepository.save(profile));
                }).orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/education")
    public ResponseEntity<Profile> modifyEducation(@PathVariable String id, @RequestBody @Valid Education.ModificationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getEducation().stream().filter(e -> e.getId().equals(transfer.getId())).findAny()
                        .map(education -> {
                            transfer.merge(education);
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(EDUCATION)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/education/{iid}")
    public ResponseEntity<Profile> deleteEducation(@PathVariable String id, @PathVariable String iid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getEducation().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(education -> {
                            profile.getEducation().remove(education);
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(EDUCATION)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/education/{iid}/institution/{iiid}")
    public ResponseEntity<Profile> setEducationInstitution(@PathVariable String id, @PathVariable String
            iid, @PathVariable String iiid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getEducation().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(education -> institutionRepository.findById(iiid)
                                .map(institution -> {
                                    education.setInstitution(institution);
                                    return ResponseEntity.ok(profileRepository.save(profile));
                                }).orElseGet(() -> security.notFound(INSTITUTION)))
                        .orElseGet(() -> security.notFound(EDUCATION)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/education/{iid}/institution")
    public ResponseEntity<Profile> unsetEducationInstitution(@PathVariable String id, @PathVariable String iid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getEducation().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(education -> {
                            education.setInstitution(null);
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(EDUCATION)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/education/{iid}/specialty/{iiid}")
    public ResponseEntity<Profile> setEducationSpecialty(@PathVariable String id, @PathVariable String
            iid, @PathVariable String iiid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getEducation().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(education -> specialtyRepository.findById(iiid)
                                .map(specialty -> {
                                    education.setSpecialty(specialty);
                                    return ResponseEntity.ok(profileRepository.save(profile));
                                }).orElseGet(() -> security.notFound(SPECIALTY)))
                        .orElseGet(() -> security.notFound(EDUCATION)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/education/{iid}/specialty")
    public ResponseEntity<Profile> unsetEducationSpecialty(@PathVariable String id, @PathVariable String iid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getEducation().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(education -> {
                            education.setSpecialty(null);
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(EDUCATION)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @PostMapping("{id}/education/{iid}/point")
    public ResponseEntity<Profile> addEducationPoint(@PathVariable String id, @PathVariable String
            iid, @RequestBody @Valid Point.CreationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getEducation().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(education -> {
                            education.getPoints().add(transfer.merge(new Point()));
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(EDUCATION))
                ).orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/education/{iid}/point")
    public ResponseEntity<Profile> modifyEducationPoint(@PathVariable String id, @PathVariable String
            iid, @RequestBody @Valid Point.ModificationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getEducation().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(education -> education.getPoints().stream().filter(p -> p.getId().equals(transfer.getId())).findAny()
                                .map(point -> {
                                    transfer.merge(point);
                                    return ResponseEntity.ok(profileRepository.save(profile));
                                }).orElseGet(() -> security.notFound(POINT)))
                        .orElseGet(() -> security.notFound(EDUCATION)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/education/{iid}/point/{iiid}")
    public ResponseEntity<Profile> deleteEducationPoint(@PathVariable String id, @PathVariable String
            iid, @PathVariable String iiid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getEducation().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(education -> education.getPoints().stream().filter(p -> p.getId().equals(iiid)).findAny()
                                .map(point -> {
                                    education.getPoints().remove(point);
                                    return ResponseEntity.ok(profileRepository.save(profile));
                                }).orElseGet(() -> security.notFound(POINT)))
                        .orElseGet(() -> security.notFound(EDUCATION)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }


    @PostMapping("{id}/additional")
    public ResponseEntity<Profile> addAdditional(@PathVariable String id, @RequestBody @Valid Additional.CreationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> {
                    profile.getAdditional().add(transfer.merge(new Additional()));
                    return ResponseEntity.ok(profileRepository.save(profile));
                }).orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/additional")
    public ResponseEntity<Profile> modifyAdditional(@PathVariable String
                                                            id, @RequestBody @Valid Additional.ModificationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getAdditional().stream().filter(e -> e.getId().equals(transfer.getId())).findAny()
                        .map(additional -> {
                            transfer.merge(additional);
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(ADDITIONAL)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/additional/{iid}")
    public ResponseEntity<Profile> deleteAdditional(@PathVariable String id, @PathVariable String iid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getAdditional().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(additional -> {
                            profile.getAdditional().remove(additional);
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(ADDITIONAL)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @PostMapping("{id}/additional/{iid}/point")
    public ResponseEntity<Profile> addAdditionalPoint(@PathVariable String id, @PathVariable String
            iid, @RequestBody @Valid Point.CreationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getAdditional().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(additional -> {
                            additional.getPoints().add(transfer.merge(new Point()));
                            return ResponseEntity.ok(profileRepository.save(profile));
                        }).orElseGet(() -> security.notFound(ADDITIONAL))
                ).orElseGet(() -> security.notFound(PROFILE)));
    }

    @PutMapping("{id}/additional/{iid}/point")
    public ResponseEntity<Profile> modifyAdditionalPoint(@PathVariable String id, @PathVariable String
            iid, @RequestBody @Valid Point.ModificationTransfer transfer) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getAdditional().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(additional -> additional.getPoints().stream().filter(p -> p.getId().equals(transfer.getId())).findAny()
                                .map(point -> {
                                    transfer.merge(point);
                                    return ResponseEntity.ok(profileRepository.save(profile));
                                }).orElseGet(() -> security.notFound(POINT)))
                        .orElseGet(() -> security.notFound(ADDITIONAL)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

    @DeleteMapping("{id}/additional/{iid}/point/{iiid}")
    public ResponseEntity<Profile> deleteAdditionalPoint(@PathVariable String id, @PathVariable String
            iid, @PathVariable String iiid) {
        return security.checkId(id, token -> profileRepository.findById(id)
                .map(profile -> profile.getAdditional().stream().filter(e -> e.getId().equals(iid)).findAny()
                        .map(additional -> additional.getPoints().stream().filter(p -> p.getId().equals(iiid)).findAny()
                                .map(point -> {
                                    additional.getPoints().remove(point);
                                    return ResponseEntity.ok(profileRepository.save(profile));
                                }).orElseGet(() -> security.notFound(POINT)))
                        .orElseGet(() -> security.notFound(ADDITIONAL)))
                .orElseGet(() -> security.notFound(PROFILE)));
    }

}
