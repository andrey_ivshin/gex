package com.gex.emp.api.controller;

import com.gex.emp.persistence.entity.Skill;
import com.gex.emp.persistence.repository.SkillRepository;
import com.gex.sec.api.security.SecurityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/skill")
@RequiredArgsConstructor
public class SkillController {

    public static final String SKILL = "skill";

    private final SecurityUtil security;
    private final SkillRepository skillRepository;

    @GetMapping
    public ResponseEntity<List<Skill>> getSkills() {
        return security.authorized(token -> ResponseEntity.ok(skillRepository.findAll()));
    }

    @GetMapping("{id}")
    public ResponseEntity<Skill> getSkill(@PathVariable String id) {
        return security.authorized(token -> skillRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> security.notFound(SKILL)));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteSkill(@PathVariable String id) {
        return security.admin(token -> skillRepository.findById(id)
                .map(skill -> {
                    skillRepository.deleteById(id);
                    return ResponseEntity.ok().<Void>build();
                }).orElseGet(() -> security.notFound(SKILL)));
    }

    @PostMapping
    public ResponseEntity<Skill> createSkill(@RequestBody @Valid Skill.CreationTransfer transfer) {
        return security.admin(token -> ResponseEntity.ok(skillRepository.save(transfer.merge(new Skill()))));
    }

    @PutMapping
    public ResponseEntity<Skill> modifySkill(@RequestBody @Valid Skill.ModificationTransfer transfer) {
        return security.admin(token -> skillRepository.findById(transfer.getId())
                .map(skill -> ResponseEntity.ok(skillRepository.save(transfer.merge(new Skill()))))
                .orElseGet(() -> security.notFound(SKILL)));
    }

}
