package com.gex.emp.api.controller;

import com.gex.emp.persistence.entity.Language;
import com.gex.emp.persistence.repository.LanguageRepository;
import com.gex.sec.api.security.SecurityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/language")
@RequiredArgsConstructor
public class LanguageController {

    public static final String LANGUAGE = "language";

    private final SecurityUtil security;
    private final LanguageRepository languageRepository;

    @GetMapping
    public ResponseEntity<List<Language>> getLanguages() {
        return security.authorized(token -> ResponseEntity.ok(languageRepository.findAll()));
    }

    @GetMapping("{id}")
    public ResponseEntity<Language> getLanguage(@PathVariable String id) {
        return security.authorized(token -> languageRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> security.notFound(LANGUAGE)));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteLanguage(@PathVariable String id) {
        return security.admin(token -> languageRepository.findById(id)
                .map(language -> {
                    languageRepository.deleteById(id);
                    return ResponseEntity.ok().<Void>build();
                }).orElseGet(() -> security.notFound(LANGUAGE)));
    }

    @PostMapping
    public ResponseEntity<Language> createLanguage(@RequestBody @Valid Language.CreationTransfer transfer) {
        return security.admin(token -> ResponseEntity.ok(languageRepository.save(transfer.merge(new Language()))));
    }

    @PutMapping
    public ResponseEntity<Language> modifyLanguage(@RequestBody @Valid Language.ModificationTransfer transfer) {
        return security.admin(token -> languageRepository.findById(transfer.getId())
                .map(language -> ResponseEntity.ok(languageRepository.save(transfer.merge(new Language()))))
                .orElseGet(() -> security.notFound(LANGUAGE)));
    }

}
