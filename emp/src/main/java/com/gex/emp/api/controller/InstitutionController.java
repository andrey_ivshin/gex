package com.gex.emp.api.controller;

import com.gex.emp.persistence.entity.Institution;
import com.gex.emp.persistence.repository.InstitutionRepository;
import com.gex.sec.api.security.SecurityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/institution")
@RequiredArgsConstructor
public class InstitutionController {

    public static final String INSTITUTION = "institution";

    private final SecurityUtil security;
    private final InstitutionRepository institutionRepository;

    @GetMapping
    public ResponseEntity<List<Institution>> getInstitutions() {
        return security.authorized(token -> ResponseEntity.ok(institutionRepository.findAll()));
    }

    @GetMapping("{id}")
    public ResponseEntity<Institution> getInstitution(@PathVariable String id) {
        return security.authorized(token -> institutionRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> security.notFound(INSTITUTION)));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteInstitution(@PathVariable String id) {
        return security.admin(token -> institutionRepository.findById(id)
                .map(institution -> {
                    institutionRepository.deleteById(id);
                    return ResponseEntity.ok().<Void>build();
                }).orElseGet(() -> security.notFound(INSTITUTION)));
    }

    @PostMapping
    public ResponseEntity<Institution> createInstitution(@RequestBody @Valid Institution.CreationTransfer transfer) {
        return security.admin(token -> ResponseEntity.ok(institutionRepository.save(transfer.merge(new Institution()))));
    }

    @PutMapping
    public ResponseEntity<Institution> modifyInstitution(@RequestBody @Valid Institution.ModificationTransfer transfer) {
        return security.admin(token -> institutionRepository.findById(transfer.getId())
                .map(institution -> ResponseEntity.ok(institutionRepository.save(transfer.merge(new Institution()))))
                .orElseGet(() -> security.notFound(INSTITUTION)));
    }

}
