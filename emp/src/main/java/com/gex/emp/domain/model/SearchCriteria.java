package com.gex.emp.domain.model;

import com.gex.emp.persistence.entity.Language;
import com.gex.emp.persistence.entity.Skill;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SearchCriteria {

    List<PositionSearchCriteria> positions;
    List<SpecialtySearchCriteria> specialties;
    List<SkillSearchCriteria> skills;
    List<LanguageSearchCriteria> languages;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class PositionSearchCriteria {

        @NotNull
        String positionId;

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class SpecialtySearchCriteria {

        @NotNull
        String specialtyId;

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class SkillSearchCriteria {

        @NotNull
        String skillId;
        @NotNull
        Skill.Level skillLevel;

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class LanguageSearchCriteria {

        @NotNull
        String languageId;
        @NotNull
        Language.Level languageLevel;

    }

}
