package com.gex.emp.domain.service;

import java.util.Arrays;
import java.util.stream.Collectors;

public class JsonMapKeyMapper {

    private static final String DELIMITER = ",";

    private JsonMapKeyMapper() {
    }

    public static <T> String map(T t) {
        return Arrays.stream(t.getClass().getDeclaredFields()).map(field -> {
            boolean access = field.canAccess(t);
            field.setAccessible(true);
            String value = "";
            try {
                value = field.get(t).toString();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            field.setAccessible(access);
            return value;
        }).collect(Collectors.joining(DELIMITER));
    }

}
