package com.gex.emp.persistence.entity;

import com.gex.emp.domain.service.IdGenerator;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "company_tab")
public class Company implements Serializable {

    @Id
    String id;
    String name;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class CreationTransfer implements Serializable {

        @NotNull @Size(min = 1, max = 1000)
        String name;

        public Company merge(Company company) {
            company.setId(IdGenerator.generate());
            company.setName(name);
            return company;
        }

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class ModificationTransfer implements Serializable {

        @NotNull
        String id;
        @NotNull @Size(min = 1, max = 1000)
        String name;

        public Company merge(Company company) {
            company.setId(id);
            company.setName(name);
            return company;
        }

    }

}
