package com.gex.emp.persistence.repository;

import com.gex.emp.domain.model.SearchCriteria;
import com.gex.emp.persistence.entity.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class ProfileSearchRepository {

    private final EntityManager entityManager;

    @Transactional
    public List<Profile> find(SearchCriteria searchCriteria) {
        var builder = entityManager.getCriteriaBuilder();
        var query = builder.createQuery(Profile.class);
        var profile = query.from(Profile.class);
        query.where(builder.and(getPredicates(searchCriteria, profile, builder)));
        Set<Profile> profiles = new HashSet<>(entityManager.createQuery(query).getResultList());
        return Arrays.asList(profiles.toArray(new Profile[0]));
    }

    private Predicate[] getPredicates(SearchCriteria searchCriteria, Root<Profile> profile, CriteriaBuilder builder) {
        List<Predicate> predicates = new LinkedList<>();
        setPositionsPredicate(searchCriteria.getPositions(), profile, predicates);
        setSpecialtiesPredicate(searchCriteria.getSpecialties(), profile, predicates);
        setSkillsPredicate(searchCriteria.getSkills(), profile, builder, predicates);
        setLanguagesPredicate(searchCriteria.getLanguages(), profile, builder, predicates);
        return predicates.toArray(Predicate[]::new);
    }

    private void setPositionsPredicate(List<SearchCriteria.PositionSearchCriteria> positions, Root<Profile> profile,
                                       List<Predicate> predicates) {
        if (!positions.isEmpty()) {
            Join<Object, Object> profileExperience = profile.join(Profile_.EXPERIENCE);
            Join<Object, Object> profileExperiencePosition = profileExperience.join(Experience_.POSITION);
            predicates.add(profileExperiencePosition.get(Position_.ID).in(positions.stream()
                    .map(SearchCriteria.PositionSearchCriteria::getPositionId)
                    .collect(Collectors.toList())));
        }
    }

    private void setSpecialtiesPredicate(List<SearchCriteria.SpecialtySearchCriteria> specialties, Root<Profile> profile,
                                         List<Predicate> predicates) {
        if (!specialties.isEmpty()) {
            Join<Object, Object> profileEducation = profile.join(Profile_.EDUCATION);
            Join<Object, Object> profileEducationSpecialty = profileEducation.join(Education_.SPECIALTY);
            predicates.add(profileEducationSpecialty.get(Specialty_.ID).in(specialties.stream()
                    .map(SearchCriteria.SpecialtySearchCriteria::getSpecialtyId)
                    .collect(Collectors.toList())));
        }
    }

    private void setSkillsPredicate(List<SearchCriteria.SkillSearchCriteria> skills, Root<Profile> profile,
                                    CriteriaBuilder builder, List<Predicate> predicates) {
        if (!skills.isEmpty()) {
            String map = "((?, ?)" + ",(?, ?)".repeat(skills.size() - 1) + ")";
            var query = entityManager.createNativeQuery(
                    "select p.id " +
                            "from profile_tab p " +
                            "inner join profile_tab_skills ps on p.id = ps.profile_tab_id " +
                            "where (ps.skills_key, ps.skills) in " + map + " " +
                            "group by p.id " +
                            "having count(p.id) = ?");
            for (var i = 0; i < skills.size(); i++) {
                query.setParameter(i * 2 + 1, skills.get(i).getSkillId());
                query.setParameter(i * 2 + 2, skills.get(i).getSkillLevel().ordinal());
            }
            query.setParameter(skills.size() * 2 + 1, skills.size());
            predicates.add(profile.get(Profile_.ID).in(query.getResultList()));
        }
    }

    private void setLanguagesPredicate(List<SearchCriteria.LanguageSearchCriteria> languages, Root<Profile> profile,
                                    CriteriaBuilder builder, List<Predicate> predicates) {
        if (!languages.isEmpty()) {
            String map = "((?, ?)" + ",(?, ?)".repeat(languages.size() - 1) + ")";
            var query = entityManager.createNativeQuery(
                    "select p.id " +
                            "from profile_tab p " +
                            "inner join public.profile_tab_languages pl on p.id = pl.profile_tab_id " +
                            "where (pl.languages_key, pl.languages) in " + map + " " +
                            "group by p.id " +
                            "having count(p.id) = ?");
            for (var i = 0; i < languages.size(); i++) {
                query.setParameter(i * 2 + 1, languages.get(i).getLanguageId());
                query.setParameter(i * 2 + 2, languages.get(i).getLanguageLevel().ordinal());
            }
            query.setParameter(languages.size() * 2 + 1, languages.size());
            predicates.add(profile.get(Profile_.ID).in(query.getResultList()));
        }
    }

}
