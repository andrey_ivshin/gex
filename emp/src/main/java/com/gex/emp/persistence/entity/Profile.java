package com.gex.emp.persistence.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "profile_tab")
public class Profile implements Serializable {

    @Id
    String id;
    String name;
    String about;
    @ManyToOne
    Position position;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    List<Contact> contacts;
    @ElementCollection
    Map<Skill, Skill.Level> skills;
    @ElementCollection
    Map<Language, Language.Level> languages;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    List<Experience> experience;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    List<Education> education;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    List<Additional> additional;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class Transfer implements Serializable {

        @NotNull
        String id;
        @NotNull @Size(min = 1, max = 1000)
        String name;
        @NotNull @Size(min = 1, max = 1000)
        String about;

        public Profile merge(Profile profile) {
            profile.setId(id);
            profile.setName(name);
            profile.setAbout(about);
            return profile;
        }

    }

}
