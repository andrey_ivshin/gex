package com.gex.emp.persistence.repository;

import com.gex.emp.persistence.entity.Position;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PositionRepository extends JpaRepository<Position, String> {
}
