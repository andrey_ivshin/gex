package com.gex.emp.persistence.repository;

import com.gex.emp.persistence.entity.Institution;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstitutionRepository extends JpaRepository<Institution, String> {
}
