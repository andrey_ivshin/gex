package com.gex.emp.persistence.repository;

import com.gex.emp.persistence.entity.Specialty;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecialtyRepository extends JpaRepository<Specialty, String> {
}
