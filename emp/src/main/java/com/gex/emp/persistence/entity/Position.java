package com.gex.emp.persistence.entity;

import com.gex.emp.domain.service.IdGenerator;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "position_tab")
public class Position implements Serializable {

    @Id
    String id;
    String name;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class CreationTransfer implements Serializable {

        @NotNull @Size(min = 1, max = 1000)
        String name;

        public Position merge(Position position) {
            position.setId(IdGenerator.generate());
            position.setName(name);
            return position;
        }

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class ModificationTransfer implements Serializable {

        @NotNull
        String id;
        @NotNull @Size(min = 1, max = 1000)
        String name;

        public Position merge(Position position) {
            position.setId(id);
            position.setName(name);
            return position;
        }

    }

}
