package com.gex.emp.persistence.entity;

import com.gex.emp.domain.service.IdGenerator;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "additional_tab")
public class Additional implements Serializable {

    @Id
    String id;
    String name;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    List<Point> points;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class CreationTransfer implements Serializable {

        @NotNull @Size(min = 1, max = 1000)
        String name;

        public Additional merge(Additional additional) {
            additional.setId(IdGenerator.generate());
            additional.setName(name);
            return additional;
        }

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class ModificationTransfer implements Serializable {

        @NotNull
        String id;
        @NotNull @Size(min = 1, max = 1000)
        String name;

        public Additional merge(Additional additional) {
            additional.setId(id);
            additional.setName(name);
            return additional;
        }

    }

}
