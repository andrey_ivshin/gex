package com.gex.emp.persistence.entity;

import com.gex.emp.domain.service.IdGenerator;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "experience_tab")
public class Experience implements Serializable {

    @Id
    String id;
    @ManyToOne
    Company company;
    @ManyToOne
    Position position;
    @Temporal(TemporalType.DATE)
    Date start;
    @Temporal(TemporalType.DATE)
    Date stop;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    List<Point> points;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class CreationTransfer implements Serializable {

        @NotNull
        Date start;
        Date stop;

        public Experience merge(Experience experience) {
            experience.setId(IdGenerator.generate());
            experience.setStart(start);
            experience.setStop(stop);
            return experience;
        }

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class ModificationTransfer implements Serializable {

        @NotNull
        String id;
        @NotNull
        Date start;
        Date stop;

        public Experience merge(Experience experience) {
            experience.setId(id);
            experience.setStart(start);
            experience.setStop(stop);
            return experience;
        }

    }

}
