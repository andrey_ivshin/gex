package com.gex.emp.persistence.repository;

import com.gex.emp.persistence.entity.Point;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PointRepository extends JpaRepository<Point, String> {
}
