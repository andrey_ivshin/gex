package com.gex.emp.persistence.repository;

import com.gex.emp.persistence.entity.Language;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LanguageRepository extends JpaRepository<Language, String> {
}
