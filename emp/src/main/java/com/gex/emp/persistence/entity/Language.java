package com.gex.emp.persistence.entity;

import com.gex.emp.domain.service.IdGenerator;
import com.gex.emp.domain.service.JsonMapKeyMapper;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "language_tab")
public class Language implements Serializable {

    @Id
    String id;
    String name;

    @Override
    public String toString() {
        return JsonMapKeyMapper.map(this);
    }

    public enum Level implements Serializable {
        A1, A2, B1, B2, C1, C2, NATIVE
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class LanguageLevelTransfer implements Serializable {

        @NotNull
        Language.Level level;

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class CreationTransfer implements Serializable {

        @NotNull @Size(min = 1, max = 1000)
        String name;

        public Language merge(Language language) {
            language.setId(IdGenerator.generate());
            language.setName(name);
            return language;
        }

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class ModificationTransfer implements Serializable {

        @NotNull
        String id;
        @NotNull @Size(min = 1, max = 1000)
        String name;

        public Language merge(Language language) {
            language.setId(id);
            language.setName(name);
            return language;
        }

    }

}
