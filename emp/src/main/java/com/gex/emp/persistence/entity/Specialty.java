package com.gex.emp.persistence.entity;

import com.gex.emp.domain.service.IdGenerator;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "specialty_tab")
public class Specialty implements Serializable {

    @Id
    String id;
    String name;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class CreationTransfer implements Serializable {

        @NotNull @Size(min = 1, max = 1000)
        String name;

        public Specialty merge(Specialty specialty) {
            specialty.setId(IdGenerator.generate());
            specialty.setName(name);
            return specialty;
        }

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class ModificationTransfer implements Serializable {

        @NotNull
        String id;
        @NotNull @Size(min = 1, max = 1000)
        String name;

        public Specialty merge(Specialty specialty) {
            specialty.setId(id);
            specialty.setName(name);
            return specialty;
        }

    }

}
