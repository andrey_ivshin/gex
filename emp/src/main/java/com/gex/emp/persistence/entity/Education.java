package com.gex.emp.persistence.entity;

import com.gex.emp.domain.service.IdGenerator;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "education_tab")
public class Education implements Serializable {

    @Id
    String id;
    @ManyToOne
    Institution institution;
    @ManyToOne
    Specialty specialty;
    @Temporal(TemporalType.DATE)
    Date start;
    @Temporal(TemporalType.DATE)
    Date stop;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    List<Point> points;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class CreationTransfer implements Serializable {

        @NotNull
        Date start;
        Date stop;

        public Education merge(Education education) {
            education.setId(IdGenerator.generate());
            education.setStart(start);
            education.setStop(stop);
            return education;
        }

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class ModificationTransfer implements Serializable {

        @NotNull
        String id;
        @NotNull
        Date start;
        Date stop;

        public Education merge(Education education) {
            education.setId(id);
            education.setStart(start);
            education.setStop(stop);
            return education;
        }

    }

}
