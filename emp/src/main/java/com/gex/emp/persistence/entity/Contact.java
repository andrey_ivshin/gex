package com.gex.emp.persistence.entity;

import com.gex.emp.domain.service.IdGenerator;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "contact_tab")
public class Contact implements Serializable {

    @Id
    String id;
    String value;
    @Enumerated(EnumType.STRING)
    Type type;

    public enum Type implements Serializable {
        ADDRESS, EMAIL, PHONE
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class CreationTransfer implements Serializable {

        @NotNull @Size(min = 1, max = 1000)
        String value;
        @NotNull
        Type type;

        public Contact merge(Contact contact) {
            contact.setId(IdGenerator.generate());
            contact.setValue(value);
            contact.setType(type);
            return contact;
        }

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class ModificationTransfer implements Serializable {

        @NotNull
        String id;
        @NotNull @Size(min = 1, max = 1000)
        String value;
        @NotNull
        Type type;

        public Contact merge(Contact contact) {
            contact.setId(id);
            contact.setValue(value);
            contact.setType(type);
            return contact;
        }

    }

}
