package com.gex.emp.persistence.repository;

import com.gex.emp.persistence.entity.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileRepository extends JpaRepository<Profile, String> {

}
