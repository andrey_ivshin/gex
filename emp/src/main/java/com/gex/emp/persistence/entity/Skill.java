package com.gex.emp.persistence.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.gex.emp.domain.service.IdGenerator;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import static com.gex.emp.domain.service.JsonMapKeyMapper.map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "skill_tab")
public class Skill implements Serializable {

    @Id
    String id;
    String name;

    @Override
    public String toString() {
        return map(this);
    }

    public enum Level implements Serializable {

        BASIC, NOVICE, INTERMEDIATE, ADVANCED, EXPERT;

        @JsonCreator
        @Override
        public String toString() {
            return super.toString();
        }

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class LevelTransfer implements Serializable {

        @NotNull
        Level level;

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class CreationTransfer implements Serializable {

        @NotNull @Size(min = 1, max = 1000)
        String name;

        public Skill merge(Skill skill) {
            skill.setId(IdGenerator.generate());
            skill.setName(name);
            return skill;
        }

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class ModificationTransfer implements Serializable {

        @NotNull
        String id;
        @NotNull @Size(min = 1, max = 1000)
        String name;

        public Skill merge(Skill skill) {
            skill.setId(id);
            skill.setName(name);
            return skill;
        }

    }

}
