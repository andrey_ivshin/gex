create table additional_tab
(
    id   varchar(255) not null,
    name varchar(255),
    primary key (id)
);
create table additional_tab_points
(
    additional_tab_id varchar(255) not null,
    points_id         varchar(255) not null
);
create table company_tab
(
    id   varchar(255) not null,
    name varchar(255),
    primary key (id)
);
create table contact_tab
(
    id    varchar(255) not null,
    type  varchar(255),
    value varchar(255),
    primary key (id)
);
create table education_tab
(
    id             varchar(255) not null,
    start          date,
    stop           date,
    institution_id varchar(255),
    specialty_id   varchar(255),
    primary key (id)
);
create table education_tab_points
(
    education_tab_id varchar(255) not null,
    points_id        varchar(255) not null
);
create table experience_tab
(
    id          varchar(255) not null,
    start       date,
    stop        date,
    company_id  varchar(255),
    position_id varchar(255),
    primary key (id)
);
create table experience_tab_points
(
    experience_tab_id varchar(255) not null,
    points_id         varchar(255) not null
);
create table institution_tab
(
    id   varchar(255) not null,
    name varchar(255),
    primary key (id)
);
create table language_tab
(
    id   varchar(255) not null,
    name varchar(255),
    primary key (id)
);
create table point_tab
(
    id      varchar(255) not null,
    content varchar(255),
    primary key (id)
);
create table position_tab
(
    id   varchar(255) not null,
    name varchar(255),
    primary key (id)
);
create table profile_tab
(
    id          varchar(255) not null,
    about       varchar(255),
    name        varchar(255),
    position_id varchar(255),
    primary key (id)
);
create table profile_tab_additional
(
    profile_tab_id varchar(255) not null,
    additional_id  varchar(255) not null
);
create table profile_tab_contacts
(
    profile_tab_id varchar(255) not null,
    contacts_id    varchar(255) not null
);
create table profile_tab_education
(
    profile_tab_id varchar(255) not null,
    education_id   varchar(255) not null
);
create table profile_tab_experience
(
    profile_tab_id varchar(255) not null,
    experience_id  varchar(255) not null
);
create table profile_tab_languages
(
    profile_tab_id varchar(255) not null,
    languages      int4,
    languages_key  varchar(255) not null,
    primary key (profile_tab_id, languages_key)
);
create table profile_tab_skills
(
    profile_tab_id varchar(255) not null,
    skills         int4,
    skills_key     varchar(255) not null,
    primary key (profile_tab_id, skills_key)
);
create table skill_tab
(
    id   varchar(255) not null,
    name varchar(255),
    primary key (id)
);
create table specialty_tab
(
    id   varchar(255) not null,
    name varchar(255),
    primary key (id)
);
alter table if exists additional_tab_points drop constraint if exists UK_44h518wa4m0tfw1dcfats2en0;
alter table if exists additional_tab_points add constraint UK_44h518wa4m0tfw1dcfats2en0 unique (points_id);
alter table if exists education_tab_points drop constraint if exists UK_1mco1pu2tefkb3lrf6x4b0677;
alter table if exists education_tab_points add constraint UK_1mco1pu2tefkb3lrf6x4b0677 unique (points_id);
alter table if exists experience_tab_points drop constraint if exists UK_8hsrtu58xgrvdmwlgewlbtqwe;
alter table if exists experience_tab_points add constraint UK_8hsrtu58xgrvdmwlgewlbtqwe unique (points_id);
alter table if exists profile_tab_additional drop constraint if exists UK_hpbuh2jsabkx957vv2rg9xqfj;
alter table if exists profile_tab_additional add constraint UK_hpbuh2jsabkx957vv2rg9xqfj unique (additional_id);
alter table if exists profile_tab_contacts drop constraint if exists UK_aqevun3lwfirj4vg6dwujntvx;
alter table if exists profile_tab_contacts add constraint UK_aqevun3lwfirj4vg6dwujntvx unique (contacts_id);
alter table if exists profile_tab_education drop constraint if exists UK_rvi4y5qslqnrrgvnuitbrw690;
alter table if exists profile_tab_education add constraint UK_rvi4y5qslqnrrgvnuitbrw690 unique (education_id);
alter table if exists profile_tab_experience drop constraint if exists UK_rxve4fota443n2mdbi4i67168;
alter table if exists profile_tab_experience add constraint UK_rxve4fota443n2mdbi4i67168 unique (experience_id);
alter table if exists additional_tab_points add constraint FK37rhpwbimbita38tgaabivetm foreign key (points_id) references point_tab;
alter table if exists additional_tab_points add constraint FKfb7lfeqh61negnqpjxjy2kjsj foreign key (additional_tab_id) references additional_tab;
alter table if exists education_tab add constraint FK950l2dj1pl4trwr696o0hj0pe foreign key (institution_id) references institution_tab;
alter table if exists education_tab add constraint FKg5hub6ou0y9ccaemvusda8gk0 foreign key (specialty_id) references specialty_tab;
alter table if exists education_tab_points add constraint FKc2hw15rka1bcr462ua3kksi7d foreign key (points_id) references point_tab;
alter table if exists education_tab_points add constraint FK7k4fkqr00db5428d8uy5kwx3j foreign key (education_tab_id) references education_tab;
alter table if exists experience_tab add constraint FKghwoa6ya3pudnbh85rc37spyk foreign key (company_id) references company_tab;
alter table if exists experience_tab add constraint FKekq8rluxekifj2rns4l5bfnk0 foreign key (position_id) references position_tab;
alter table if exists experience_tab_points add constraint FKm4cn4ec0j06toanxk4cksg8p4 foreign key (points_id) references point_tab;
alter table if exists experience_tab_points add constraint FKjkljaoi3k2y42qlc5dieuixee foreign key (experience_tab_id) references experience_tab;
alter table if exists profile_tab add constraint FK50cstkb2s0emuou3g1q05osso foreign key (position_id) references position_tab;
alter table if exists profile_tab_additional add constraint FKftdpvlk16qlnmvip3j1vp1pf2 foreign key (additional_id) references additional_tab;
alter table if exists profile_tab_additional add constraint FKhhqkwu10ruqw0y2232d92o19x foreign key (profile_tab_id) references profile_tab;
alter table if exists profile_tab_contacts add constraint FKb9tjdwq85qmh90kgojsjrlbml foreign key (contacts_id) references contact_tab;
alter table if exists profile_tab_contacts add constraint FKq703c8hs00brtvkq9ymlugy7r foreign key (profile_tab_id) references profile_tab;
alter table if exists profile_tab_education add constraint FKube5r26ksqvt3gejdt8n9q2f foreign key (education_id) references education_tab;
alter table if exists profile_tab_education add constraint FK5ic1mathv1940o517ot86hion foreign key (profile_tab_id) references profile_tab;
alter table if exists profile_tab_experience add constraint FK6d4ihy7ir8cuquy09xxghy8ei foreign key (experience_id) references experience_tab;
alter table if exists profile_tab_experience add constraint FKpwglg0vj513e9r97worry563x foreign key (profile_tab_id) references profile_tab;
alter table if exists profile_tab_languages add constraint FK7fnuo8ykhl7l0q7x5bvvecjs8 foreign key (languages_key) references language_tab;
alter table if exists profile_tab_languages add constraint FKrwuim8kxsvgbpsv03ilnl0ybg foreign key (profile_tab_id) references profile_tab;
alter table if exists profile_tab_skills add constraint FKelc84dwab9c369v2st8mrhfif foreign key (skills_key) references skill_tab;
alter table if exists profile_tab_skills add constraint FKr8cm75kxg0rpa1rhb49dtyhm4 foreign key (profile_tab_id) references profile_tab;