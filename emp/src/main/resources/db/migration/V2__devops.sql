insert into skill_tab (name, id)
values ('Java', '7f1b5fb4-2d28-450a-88da-68bc66dad997');

insert into skill_tab (name, id)
values ('Linux', 'eead6b56-b85d-4efe-9917-a9372f5eb9d8');

insert into language_tab (name, id)
values ('English', '01cce72a-751d-4067-89e4-0ca0f700be9f');

insert into language_tab (name, id)
values ('Russian', '5a4f39da-b45d-45ff-8341-81fda029d1dc');

insert into position_tab (name, id)
values ('DevOps Engineer', 'd4ca7aa9-2ce7-45fa-bb77-a53b6bf9ba6f');

insert into position_tab (name, id)
values ('Java Developer', '5687423a-df24-4c73-a546-2064f501ad41');

insert into specialty_tab (name, id)
values ('Programmer', 'bdb0bb25-6b62-4f19-b843-8b3c960513d5');

insert into specialty_tab (name, id)
values ('Software Developer', '960e6274-dd59-49ad-a6bd-87bb6b2f60b2');

insert into company_tab (name, id)
values ('Cool Inc.', '97548a25-cd2d-48d4-b166-c3504b3fe1ff');

insert into institution_tab (name, id)
values ('Cool University', '3e8e47e1-7a35-417d-a343-10e658717ea1');