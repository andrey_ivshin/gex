package com.gex.sec.api.security;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Properties;

@Component
public class PropertyHolder {

    private static final String PROPERTY_FILE = "jwt.properties";
    public static final String SECRET = "jwt.secret";

    private final Properties properties = new Properties();

    public PropertyHolder() {
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream(PROPERTY_FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

}
