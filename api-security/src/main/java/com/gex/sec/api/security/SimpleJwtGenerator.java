package com.gex.sec.api.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class SimpleJwtGenerator implements JwtGenerator {

    private String secret;

    private static final String ID = "id";
    private static final String ROLE = "role";

    private final PropertyHolder propertyHolder;

    @PostConstruct
    public void init() {
        secret = propertyHolder.getProperty(PropertyHolder.SECRET);
    }

    @Override
    public String generate(Token token) {
        var now = Instant.now();
        return Jwts.builder()
                .claim(ID, token.getId())
                .claim(ROLE, token.getRole())
                .setIssuedAt(Date.from(now))
                .setExpiration(Date.from(now.plus(30, ChronoUnit.MINUTES)))
                .signWith(Keys.hmacShaKeyFor(secret.getBytes()))
                .compact();
    }

    @Override
    public Token parse(String string) {
        var claims = Jwts.parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor(secret.getBytes()))
                .build()
                .parseClaimsJws(string)
                .getBody();
        return new Token(
                claims.get(ID, String.class),
                claims.get(ROLE, String.class)
        );
    }

}
