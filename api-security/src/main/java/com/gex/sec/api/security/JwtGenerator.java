package com.gex.sec.api.security;

public interface JwtGenerator {

    String generate(Token token);

    Token parse(String string);

}
