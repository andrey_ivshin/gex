package com.gex.sec.api.security;

import io.jsonwebtoken.JwtException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

@Component
@RequiredArgsConstructor
public class SecurityUtil {

    public static final String MESSAGE = "message";
    public static final String AUTHORIZATION = "authorization";
    public static final String ID = "id";
    public static final String ROLE = "role";
    public static final String NOT_FOUND = "not found";
    public static final String ALREADY_EXIST = "already exist";
    public static final String ALREADY_PRESENT = "already present";
    public static final int EXPIRATION = 1800;

    private final JwtGenerator jwt;

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private HttpServletResponse response;

    public <T> ResponseEntity<T> authorize(Token token) {
        setCookies(token);
        return unauthorized(() -> ResponseEntity.ok().build());
    }

    public ResponseEntity<Void> logout() {
        return authorized(token -> {
            unsetCookies();
            return ResponseEntity.ok().build();
        });
    }

    public <T> ResponseEntity<T> error(HttpStatus status, String message) {
        return ResponseEntity.status(status).header(MESSAGE, message).build();
    }

    public <T> ResponseEntity<T> unauthorized(Supplier<ResponseEntity<T>> supplier) {
        if (request.getCookies() != null
                && Arrays.stream(request.getCookies()).anyMatch(cookie -> cookie.getName().equals(AUTHORIZATION))) {
            return error(HttpStatus.FORBIDDEN, "already authorized");
        }
        return supplier.get();
    }

    public <T> ResponseEntity<T> authorized(Function<Token, ResponseEntity<T>> function) {
        if (request.getCookies() != null) {
            Optional<Cookie> optional = Arrays.stream(request.getCookies())
                    .filter(cookie -> cookie.getName().equals(AUTHORIZATION)).findAny();
            if (optional.isPresent()) {
                try {
                    var token = jwt.parse(optional.get().getValue());
                    setCookies(token);
                    return function.apply(token);
                } catch (JwtException e) {
                    unsetCookies();
                }
            }
        }
        return error(HttpStatus.UNAUTHORIZED, "unauthorized");
    }

    public <T> ResponseEntity<T> admin(Function<Token, ResponseEntity<T>> function) {
        return authorized(token -> {
            if (token.getRole().equals(Role.ADMIN.name()))
                return function.apply(token);
            else
                return error(HttpStatus.FORBIDDEN, "not admin");
        });
    }

    public <T> ResponseEntity<T> checkId(String id, Function<Token, ResponseEntity<T>> function) {
        return authorized(token -> {
            if (!token.getRole().equals(Role.ADMIN.name()) && !token.getId().equals(id))
                return error(HttpStatus.BAD_REQUEST, "not allowed");
            else
                return function.apply(token);
        });
    }

    public <T> ResponseEntity<T> notFound(String target) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).header(MESSAGE, target + " " + NOT_FOUND).build();
    }

    public <T> ResponseEntity<T> notFoundIn(String superTarget, String target) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).header(MESSAGE, target + " in " + superTarget + " " + NOT_FOUND).build();
    }

    public <T> ResponseEntity<T> alreadyPresent(String target) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).header(MESSAGE, target + " " + ALREADY_PRESENT).build();
    }

    private void setCookies(Token token) {
        response.addCookie(getAuthorizationCookie(token, EXPIRATION));
        response.addCookie(getIdCookie(token, EXPIRATION));
        response.addCookie(getRoleCookie(token, EXPIRATION));
    }

    private void unsetCookies() {
        response.addCookie(getEmptyAuthorizationCookie(0));
        response.addCookie(getEmptyIdCookie(0));
        response.addCookie(getEmptyRoleCookie(0));
    }

    private void calibrateCookie(Cookie cookie, int expiration) {
        cookie.setMaxAge(expiration);
        cookie.setPath("/");
        cookie.setDomain(System.getenv("PROFILE") != null && System.getenv("PROFILE").equals("prod")
                        ? "gex.com"
                        : "localhost");
    }

    private Cookie getAuthorizationCookie(Token token, int expiration) {
        Cookie cookie = new Cookie(AUTHORIZATION, jwt.generate(token));
        calibrateCookie(cookie, expiration);
        return cookie;
    }

    private Cookie getEmptyAuthorizationCookie(int expiration) {
        Cookie cookie = new Cookie(AUTHORIZATION, "");
        calibrateCookie(cookie, expiration);
        return cookie;
    }

    private Cookie getIdCookie(Token token, int expiration) {
        Cookie cookie = new Cookie(ID, token.getId());
        calibrateCookie(cookie, expiration);
        return cookie;
    }

    private Cookie getEmptyIdCookie(int expiration) {
        Cookie cookie = new Cookie(ID, "");
        calibrateCookie(cookie, expiration);
        return cookie;
    }

    private Cookie getRoleCookie(Token token, int expiration) {
        Cookie cookie = new Cookie(ROLE, token.getRole());
        calibrateCookie(cookie, expiration);
        return cookie;
    }

    private Cookie getEmptyRoleCookie(int expiration) {
        Cookie cookie = new Cookie(ROLE, "");
        calibrateCookie(cookie, expiration);
        return cookie;
    }

}
