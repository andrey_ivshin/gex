package com.gex.sec.api.security;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

@FunctionalInterface
public interface Executor {

    void execute();

    class ExecutorChain {

        private final List<Executor> executors = new LinkedList<>();

        public static ExecutorChain exec(Executor executor) {
            ExecutorChain chain = new ExecutorChain();
            chain.executors.add(executor);
            return chain;
        }

        public ExecutorChain and(Executor executor) {
            executors.add(executor);
            return this;
        }

        public <T> T andReturn(Supplier<T> supplier) {
            executors.forEach(Executor::execute);
            return supplier.get();
        }

    }

}
