package com.gex.sec.api.security;

public enum Role {
    USER, ADMIN
}
