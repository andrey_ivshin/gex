import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {User} from '../../model/user/user';
import {UserCreationRequest} from '../../model/user-creation-request/user-creation-request';
import {UserModificationRequest} from '../../model/user-modification-request/user-modification-request';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private http: HttpClient) {
    }

    getUsers(): Observable<User[]> {
        return this.http.get<User[]>(`${environment.sec}/api/user`);
    }

    getUser(id: string): Observable<User> {
        return this.http.get<User>(`${environment.sec}/api/user/${id}`);
    }

    createUser(user: UserCreationRequest): Observable<User> {
        return this.http.post<User>(`${environment.sec}/api/user`, user);
    }

    modifyUser(user: UserModificationRequest): Observable<User> {
        return this.http.put<User>(`${environment.sec}/api/user`, user);
    }

    removeUser(id: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${environment.sec}/api/user/${id}`, {observe: 'response'});
    }

}
