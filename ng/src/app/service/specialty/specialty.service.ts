import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Specialty} from '../../model/specialty/specialty';
import {environment} from '../../../environments/environment';
import {SpecialtyCreationTransfer} from '../../model/specialty-creation-transfer/specialty-creation-transfer';
import {SpecialtyModificationTransfer} from '../../model/specialty-modification-transfer/specialty-modification-transfer';

@Injectable({
    providedIn: 'root'
})
export class SpecialtyService {

    constructor(private http: HttpClient) {
    }

    getSpecialties(): Observable<Specialty[]> {
        return this.http.get<Specialty[]>(`${environment.emp}/api/specialty`);
    }

    getSpecialty(id: string): Observable<Specialty> {
        return this.http.get<Specialty>(`${environment.emp}/api/specialty/${id}`);
    }

    createSpecialty(specialty: SpecialtyCreationTransfer): Observable<Specialty> {
        return this.http.post<Specialty>(`${environment.emp}/api/specialty`, specialty);
    }

    modifySpecialty(specialty: SpecialtyModificationTransfer): Observable<Specialty> {
        return this.http.put<Specialty>(`${environment.emp}/api/specialty`, specialty);
    }

    removeSpecialty(id: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${environment.emp}/api/specialty/${id}`, {observe: 'response'});
    }

}
