import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {Institution} from "../../model/institution/institution";
import {environment} from "../../../environments/environment";
import {InstitutionCreationTransfer} from "../../model/institution-creation-transfer/institution-creation-transfer";
import {InstitutionModificationTransfer} from "../../model/institution-modification-transfer/institution-modification-transfer";

@Injectable({
    providedIn: 'root'
})
export class InstitutionService {

    constructor(private http: HttpClient) {
    }

    getInstitutions(): Observable<Institution[]> {
        return this.http.get<Institution[]>(`${environment.emp}/api/institution`);
    }

    getInstitution(id: string): Observable<Institution> {
        return this.http.get<Institution>(`${environment.emp}/api/institution/${id}`);
    }

    createInstitution(institution: InstitutionCreationTransfer): Observable<Institution> {
        return this.http.post<Institution>(`${environment.emp}/api/institution`, institution);
    }

    modifyInstitution(institution: InstitutionModificationTransfer): Observable<Institution> {
        return this.http.put<Institution>(`${environment.emp}/api/institution`, institution);
    }

    removeInstitution(id: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${environment.emp}/api/institution/${id}`, {observe: 'response'});
    }

}
