import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Position} from '../../model/position/position';
import {PositionCreationTransfer} from '../../model/position-creation-transfer/position-creation-transfer';
import {PositionModificationTransfer} from '../../model/position-modification-transfer/position-modification-transfer';

@Injectable({
    providedIn: 'root'
})
export class PositionService {

    constructor(private http: HttpClient) {
    }

    getPositions(): Observable<Position[]> {
        return this.http.get<Position[]>(`${environment.emp}/api/position`);
    }

    getPosition(id: string): Observable<Position> {
        return this.http.get<Position>(`${environment.emp}/api/position/${id}`);
    }

    createPosition(position: PositionCreationTransfer): Observable<Position> {
        return this.http.post<Position>(`${environment.emp}/api/position`, position);
    }

    modifyPosition(position: PositionModificationTransfer): Observable<Position> {
        return this.http.put<Position>(`${environment.emp}/api/position`, position);
    }

    removePosition(id: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${environment.emp}/api/position/${id}`, {observe: 'response'});
    }

}
