import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Profile} from '../../model/profile/profile';
import {ProfileTransfer} from '../../model/profile-transfer/profile-transfer';
import {ContactCreationTransfer} from '../../model/contact-creation-transfer/contact-creation-transfer';
import {ContactModificationTransfer} from '../../model/contact-modification-transfer/contact-modification-transfer';
import {SkillLevelTransfer} from '../../model/skill-level-transfer/skill-level-transfer';
import {LanguageLevelTransfer} from '../../model/language-level-transfer/language-level-transfer';
import {ExperienceCreationTransfer} from '../../model/experience-creation-transfer/experience-creation-transfer';
import {ExperienceModificationTransfer} from '../../model/experience-modification-transfer/experience-modification-transfer';
import {PointCreationTransfer} from "../../model/point-creation-transfer/point-creation-transfer";
import {EducationCreationTransfer} from "../../model/education-creation-transfer/education-creation-transfer";
import {EducationModificationTransfer} from "../../model/education-modification-transfer/education-modification-transfer";
import {AdditionalCreationTransfer} from "../../model/additional-cration-transfer/additional-creation-transfer";
import {AdditionalModificationTransfer} from "../../model/additional-modification-transfer/additional-modification-transfer";
import {ProfileSearchCriteria} from "../../model/profile-search-criteria/profile-search-criteria";

@Injectable({
    providedIn: 'root'
})
export class ProfileService {

    constructor(private http: HttpClient) {
    }

    getProfiles(): Observable<Profile[]> {
        return this.http.get<Profile[]>(`${environment.emp}/api/profile`);
    }

    searchProfiles(criteria: ProfileSearchCriteria): Observable<Profile[]> {
        return this.http.post<Profile[]>(`${environment.emp}/api/profile/search`, criteria);
    }

    getProfile(id: string): Observable<Profile> {
        return this.http.get<Profile>(`${environment.emp}/api/profile/${id}`);
    }

    createProfile(profile: ProfileTransfer): Observable<Profile> {
        return this.http.post<Profile>(`${environment.emp}/api/profile`, profile);
    }

    modifyProfile(profile: ProfileTransfer): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile`, profile);
    }

    removeProfile(id: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${environment.emp}/api/profile/${id}`, {observe: 'response'});
    }

    setPosition(profileId: string, positionId: string): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/position/${positionId}`, {});
    }

    unsetPosition(profileId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/position`);
    }

    addContact(profileId: string, contact: ContactCreationTransfer): Observable<Profile> {
        return this.http.post<Profile>(`${environment.emp}/api/profile/${profileId}/contact`, contact);
    }

    modifyContact(profileId: string, contact: ContactModificationTransfer): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/contact`, contact);
    }

    removeContact(profileId: string, contactId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/contact/${contactId}`);
    }

    setProfileSkill(profileId: string, skillId: string, level: SkillLevelTransfer): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/skill/${skillId}`, level);
    }

    unsetProfileSkill(profileId: string, skillId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/skill/${skillId}`);
    }

    setProfileLanguage(profileId: string, languageId: string, level: LanguageLevelTransfer): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/language/${languageId}`, level);
    }

    unsetProfileLanguage(profileId: string, languageId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/language/${languageId}`);
    }

    addExperience(profileId: string, experience: ExperienceCreationTransfer): Observable<Profile> {
        return this.http.post<Profile>(`${environment.emp}/api/profile/${profileId}/experience`, experience);
    }

    modifyExperience(profileId: string, experience: ExperienceModificationTransfer): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/experience`, experience);
    }

    removeExperience(profileId: string, experienceId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/experience/${experienceId}`);
    }

    setExperienceCompany(profileId: string, experienceId: string, companyId: string): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/experience/${experienceId}/company/${companyId}`, {});
    }

    unsetExperienceCompany(profileId: string, experienceId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/experience/${experienceId}/company`);
    }

    setExperiencePosition(profileId: string, experienceId: string, positionId: string): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/experience/${experienceId}/position/${positionId}`, {});
    }

    unsetExperiencePosition(profileId: string, experienceId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/experience/${experienceId}/position`);
    }


    addExperiencePoint(profileId: string, experienceId: string, point: PointCreationTransfer): Observable<Profile> {
        return this.http.post<Profile>(`${environment.emp}/api/profile/${profileId}/experience/${experienceId}/point`, point);
    }

    modifyExperiencePoint(profileId: string, experienceId: string, point: PointCreationTransfer): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/experience/${experienceId}/point`, point);
    }

    removeExperiencePoint(profileId: string, experienceId: string, pointId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/experience/${experienceId}/point/${pointId}`);
    }

    addEducation(profileId: string, education: EducationCreationTransfer): Observable<Profile> {
        return this.http.post<Profile>(`${environment.emp}/api/profile/${profileId}/education`, education);
    }

    modifyEducation(profileId: string, education: EducationModificationTransfer): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/education`, education);
    }

    removeEducation(profileId: string, educationId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/education/${educationId}`);
    }

    setEducationInstitution(profileId: string, educationId: string, institutionId: string): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/education/${educationId}/institution/${institutionId}`, {});
    }

    unsetEducationInstitution(profileId: string, educationId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/education/${educationId}/institution`);
    }

    setEducationSpecialty(profileId: string, educationId: string, specialtyId: string): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/education/${educationId}/specialty/${specialtyId}`, {});
    }

    unsetEducationSpecialty(profileId: string, educationId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/education/${educationId}/specialty`);
    }


    addEducationPoint(profileId: string, educationId: string, point: PointCreationTransfer): Observable<Profile> {
        return this.http.post<Profile>(`${environment.emp}/api/profile/${profileId}/education/${educationId}/point`, point);
    }

    modifyEducationPoint(profileId: string, educationId: string, point: PointCreationTransfer): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/education/${educationId}/point`, point);
    }

    removeEducationPoint(profileId: string, educationId: string, pointId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/education/${educationId}/point/${pointId}`);
    }

    addAdditional(profileId: string, additional: AdditionalCreationTransfer): Observable<Profile> {
        return this.http.post<Profile>(`${environment.emp}/api/profile/${profileId}/additional`, additional);
    }

    modifyAdditional(profileId: string, additional: AdditionalModificationTransfer): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/additional`, additional);
    }

    removeAdditional(profileId: string, additionalId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/additional/${additionalId}`);
    }

    addAdditionalPoint(profileId: string, additionalId: string, point: PointCreationTransfer): Observable<Profile> {
        return this.http.post<Profile>(`${environment.emp}/api/profile/${profileId}/additional/${additionalId}/point`, point);
    }

    modifyAdditionalPoint(profileId: string, additionalId: string, point: PointCreationTransfer): Observable<Profile> {
        return this.http.put<Profile>(`${environment.emp}/api/profile/${profileId}/additional/${additionalId}/point`, point);
    }

    removeAdditionalPoint(profileId: string, additionalId: string, pointId: string): Observable<Profile> {
        return this.http.delete<Profile>(`${environment.emp}/api/profile/${profileId}/additional/${additionalId}/point/${pointId}`);
    }

}
