import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Language} from '../../model/language/language';
import {environment} from '../../../environments/environment';
import {LanguageCreationTransfer} from '../../model/language-creation-transfer/language-creation-transfer';
import {LanguageModificationTransfer} from '../../model/language-modification-transfer/language-modification-transfer';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

    constructor(private http: HttpClient) {
    }

    getLanguages(): Observable<Language[]> {
        return this.http.get<Language[]>(`${environment.emp}/api/language`);
    }

    getLanguage(id: string): Observable<Language> {
        return this.http.get<Language>(`${environment.emp}/api/language/${id}`);
    }

    createLanguage(language: LanguageCreationTransfer): Observable<Language> {
        return this.http.post<Language>(`${environment.emp}/api/language`, language);
    }

    modifyLanguage(language: LanguageModificationTransfer): Observable<Language> {
        return this.http.put<Language>(`${environment.emp}/api/language`, language);
    }

    removeLanguage(id: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${environment.emp}/api/language/${id}`, {observe: 'response'});
    }

}
