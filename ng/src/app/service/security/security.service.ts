import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {UserAuthorizationRequest} from '../../model/user-authorization-request/user-authorization-request';

@Injectable({
    providedIn: 'root'
})
export class SecurityService {

    constructor(private http: HttpClient) {
    }

    ID = 'id';
    ROLE = 'role';
    MESSAGE = 'message';
    NONE = 'NONE';
    ADMIN = 'ADMIN';
    timer: any;

    authorize(authorization: UserAuthorizationRequest): Observable<HttpResponse<any>> {
        return this.http.post(`${environment.sec}/api/authorize`, authorization, {observe: 'response'});
    }

    refresh(): Observable<HttpResponse<any>> {
        return this.http.post(`${environment.sec}/api/refresh`, {}, {observe: 'response'});
    }

    logout(): Observable<HttpResponse<any>> {
        return this.http.post(`${environment.sec}/api/logout`, {}, {observe: 'response'});
    }

    getErrorCode(err: any): number {
        const response = new HttpErrorResponse(err);
        return response.status;
    }

    getErrorMessage(err: any): string {
        const response = new HttpErrorResponse(err);
        return response.headers.get(this.MESSAGE) || 'unknown error';
    }

    parseRole(): string {
        const cookies = document.cookie.split('; ');
        const role = cookies.find(s => s.startsWith(this.ROLE)) || this.NONE;
        return role !== this.NONE ? role.split('=')[1] : role;
    }

    isAuthorized(): boolean {
        return this.parseRole() !== this.NONE;
    }

    isAdmin(): boolean {
        return this.parseRole() === this.ADMIN;
    }

    getId(): string {
        const cookies = document.cookie.split('; ');
        const id = cookies.find(s => s.startsWith(this.ID)) || this.NONE;
        return id !== this.NONE ? id.split('=')[1] : '';
    }

}
