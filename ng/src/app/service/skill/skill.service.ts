import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Skill} from '../../model/skill/skill';
import {environment} from '../../../environments/environment';
import {SkillCreationTransfer} from '../../model/skill-creation-transfer/skill-creation-transfer';
import {SkillModificationTransfer} from '../../model/skill-modification-transfer/skill-modification-transfer';

@Injectable({
    providedIn: 'root'
})
export class SkillService {

    constructor(private http: HttpClient) {
    }

    getSkills(): Observable<Skill[]> {
        return this.http.get<Skill[]>(`${environment.emp}/api/skill`);
    }

    getSkill(id: string): Observable<Skill> {
        return this.http.get<Skill>(`${environment.emp}/api/skill/${id}`);
    }

    createSkill(skill: SkillCreationTransfer): Observable<Skill> {
        return this.http.post<Skill>(`${environment.emp}/api/skill`, skill);
    }

    modifySkill(skill: SkillModificationTransfer): Observable<Skill> {
        return this.http.put<Skill>(`${environment.emp}/api/skill`, skill);
    }

    removeSkill(id: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${environment.emp}/api/skill/${id}`, {observe: 'response'});
    }

}
