import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {SecurityService} from '../security/security.service';

@Injectable({
    providedIn: 'root'
})
export class IdGuard implements CanActivate {

    constructor(private security: SecurityService, private router: Router) {
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if (this.security.isAdmin() || this.security.getId() === route.paramMap.get('profile-id')) {
            return true;
        } else {
            this.router.navigate(['/access-denied']);
            return false;
        }
    }

}
