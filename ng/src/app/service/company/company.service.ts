import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Company} from '../../model/company/company';
import {environment} from '../../../environments/environment';
import {CompanyCreationTransfer} from '../../model/company-creation-transfer/company-creation-transfer';
import {CompanyModificationTransfer} from '../../model/company-modification-transfer/company-modification-transfer';

@Injectable({
    providedIn: 'root'
})
export class CompanyService {

    constructor(private http: HttpClient) {
    }

    getCompanies(): Observable<Company[]> {
        return this.http.get<Company[]>(`${environment.emp}/api/company`);
    }

    getCompany(id: string): Observable<Company> {
        return this.http.get<Company>(`${environment.emp}/api/company/${id}`);
    }

    createCompany(company: CompanyCreationTransfer): Observable<Company> {
        return this.http.post<Company>(`${environment.emp}/api/company`, company);
    }

    modifyCompany(company: CompanyModificationTransfer): Observable<Company> {
        return this.http.put<Company>(`${environment.emp}/api/company`, company);
    }

    removeCompany(id: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${environment.emp}/api/company/${id}`, {observe: 'response'});
    }

}
