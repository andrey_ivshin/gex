import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HeaderComponent} from './component/.stereotype/header/header.component';
import {FooterComponent} from './component/.stereotype/footer/footer.component';
import {ContentComponent} from './component/.stereotype/content/content.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {WelcomeComponent} from './component/.stereotype/welcome/welcome.component';
import {PageNotFoundComponent} from './component/.stereotype/page-not-found/page-not-found.component';
import {LogInComponent} from './component/.stereotype/log-in/log-in.component';
import {MatCardModule} from '@angular/material/card';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {SecurityInterceptor} from './service/interceptor/security.interceptor';
import {AccessDeniedComponent} from './component/.stereotype/access-denied/access-denied.component';
import {UserListComponent} from './component/user/user-list/user-list.component';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {UserCreateComponent} from './component/user/user-create/user-create.component';
import {MatSelectModule} from '@angular/material/select';
import {UserModifyComponent} from './component/user/user-modify/user-modify.component';
import {AreYouSureComponent} from './component/.stereotype/are-you-sure/are-you-sure.component';
import {ProfileListComponent} from './component/profile/profile-list/profile-list.component';
import {ProfileCreateAdminComponent} from './component/profile/profile-create-admin/profile-create-admin.component';
import {ProfileModifyComponent} from './component/profile/profile-modify/profile-modify.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {PositionListComponent} from './component/position/position-list/position-list.component';
import {PositionCreateComponent} from './component/position/position-create/position-create.component';
import {PositionModifyComponent} from './component/position/position-modify/position-modify.component';
import {CompanyListComponent} from './component/company/company-list/company-list.component';
import {CompanyCreateComponent} from './component/company/company-create/company-create.component';
import {CompanyModifyComponent} from './component/company/company-modify/company-modify.component';
import {SpecialtyCreateComponent} from './component/specialty/specialty-create/specialty-create.component';
import {SpecialtyListComponent} from './component/specialty/specialty-list/specialty-list.component';
import {SpecialtyModifyComponent} from './component/specialty/specialty-modify/specialty-modify.component';
import {InstitutionCreateComponent} from './component/institution/institution-create/institution-create.component';
import {InstitutionListComponent} from './component/institution/institution-list/institution-list.component';
import {InstitutionModifyComponent} from './component/institution/institution-modify/institution-modify.component';
import {SkillCreateComponent} from './component/skill/skill-create/skill-create.component';
import {SkillListComponent} from './component/skill/skill-list/skill-list.component';
import {SkillModifyComponent} from './component/skill/skill-modify/skill-modify.component';
import {LanguageCreateComponent} from './component/language/language-create/language-create.component';
import {LanguageListComponent} from './component/language/language-list/language-list.component';
import {LanguageModifyComponent} from './component/language/language-modify/language-modify.component';
import {ContactCreateComponent} from './component/contact/contact-create/contact-create.component';
import {ContactModifyComponent} from './component/contact/contact-modify/contact-modify.component';
import {MatChipsModule} from '@angular/material/chips';
import { ProfileSkillAddComponent } from './component/profile-skill/profile-skill-add/profile-skill-add.component';
import { ProfileSkillModifyComponent } from './component/profile-skill/profile-skill-modify/profile-skill-modify.component';
import { ProfileLanguageAddComponent } from './component/profile-language/profile-language-add/profile-language-add.component';
import { ProfileLanguageModifyComponent } from './component/profile-language/profile-language-modify/profile-language-modify.component';
import { ExperienceCreateComponent } from './component/experience/experience-create/experience-create.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import { ExperienceModifyComponent } from './component/experience/experience-modify/experience-modify.component';
import { ProfileExperiencePositionSetComponent } from './component/profile-experience/profile-experience-position-set/profile-experience-position-set.component';
import { ProfileExperienceCompanySetComponent } from './component/profile-experience/profile-experience-company-set/profile-experience-company-set.component';
import { PointAddComponent } from './component/point/point-add/point-add.component';
import { PointModifyComponent } from './component/point/point-modify/point-modify.component';
import { EducationCreateComponent } from './component/education/education-create/education-create.component';
import { EducationModifyComponent } from './component/education/education-modify/education-modify.component';
import { ProfileEducationSpecialtySetComponent } from './component/profile-education/profile-education-specialty-set/profile-education-specialty-set.component';
import { ProfileEducationInstitutionSetComponent } from './component/profile-education/profile-education-institution-set/profile-education-institution-set.component';
import { AdditionalCreateComponent } from './component/additional/additional-create/additional-create.component';
import { AdditionalModifyComponent } from './component/additional/additional-modify/additional-modify.component';
import { ProfileShowComponent } from './component/profile/profile-show/profile-show.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        ContentComponent,
        WelcomeComponent,
        PageNotFoundComponent,
        LogInComponent,
        AccessDeniedComponent,
        UserListComponent,
        UserCreateComponent,
        UserModifyComponent,
        AreYouSureComponent,
        ProfileListComponent,
        ProfileCreateAdminComponent,
        ProfileModifyComponent,
        PositionListComponent,
        PositionCreateComponent,
        PositionModifyComponent,
        CompanyListComponent,
        CompanyCreateComponent,
        CompanyModifyComponent,
        SpecialtyCreateComponent,
        SpecialtyListComponent,
        SpecialtyModifyComponent,
        InstitutionCreateComponent,
        InstitutionListComponent,
        InstitutionModifyComponent,
        SkillCreateComponent,
        SkillListComponent,
        SkillModifyComponent,
        LanguageCreateComponent,
        LanguageListComponent,
        LanguageModifyComponent,
        ContactCreateComponent,
        ContactModifyComponent,
        ProfileSkillAddComponent,
        ProfileSkillModifyComponent,
        ProfileLanguageAddComponent,
        ProfileLanguageModifyComponent,
        ExperienceCreateComponent,
        ExperienceModifyComponent,
        ProfileExperiencePositionSetComponent,
        ProfileExperienceCompanySetComponent,
        PointAddComponent,
        PointModifyComponent,
        EducationCreateComponent,
        EducationModifyComponent,
        ProfileEducationSpecialtySetComponent,
        ProfileEducationInstitutionSetComponent,
        AdditionalCreateComponent,
        AdditionalModifyComponent,
        ProfileShowComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatToolbarModule,
        MatButtonModule,
        MatCardModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatListModule,
        MatIconModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatSelectModule,
        MatExpansionModule,
        MatSnackBarModule,
        MatChipsModule,
        MatDatepickerModule,
        MatNativeDateModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: SecurityInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
