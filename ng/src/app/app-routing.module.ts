import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from './component/.stereotype/welcome/welcome.component';
import {PageNotFoundComponent} from './component/.stereotype/page-not-found/page-not-found.component';
import {LogInComponent} from './component/.stereotype/log-in/log-in.component';
import {AccessDeniedComponent} from './component/.stereotype/access-denied/access-denied.component';
import {UnauthorizedGuard} from './service/guard/unauthorized.guard';
import {UserListComponent} from './component/user/user-list/user-list.component';
import {AdminGuard} from './service/guard/admin.guard';
import {UserCreateComponent} from './component/user/user-create/user-create.component';
import {UserModifyComponent} from './component/user/user-modify/user-modify.component';
import {ProfileListComponent} from './component/profile/profile-list/profile-list.component';
import {ProfileModifyComponent} from './component/profile/profile-modify/profile-modify.component';
import {IdGuard} from './service/guard/id.guard';
import {PositionListComponent} from './component/position/position-list/position-list.component';
import {PositionCreateComponent} from './component/position/position-create/position-create.component';
import {PositionModifyComponent} from './component/position/position-modify/position-modify.component';
import {CompanyCreateComponent} from './component/company/company-create/company-create.component';
import {CompanyModifyComponent} from './component/company/company-modify/company-modify.component';
import {CompanyListComponent} from './component/company/company-list/company-list.component';
import {SpecialtyListComponent} from './component/specialty/specialty-list/specialty-list.component';
import {SpecialtyModifyComponent} from './component/specialty/specialty-modify/specialty-modify.component';
import {SpecialtyCreateComponent} from './component/specialty/specialty-create/specialty-create.component';
import {InstitutionCreateComponent} from './component/institution/institution-create/institution-create.component';
import {InstitutionModifyComponent} from './component/institution/institution-modify/institution-modify.component';
import {InstitutionListComponent} from './component/institution/institution-list/institution-list.component';
import {SkillListComponent} from './component/skill/skill-list/skill-list.component';
import {SkillCreateComponent} from './component/skill/skill-create/skill-create.component';
import {SkillModifyComponent} from './component/skill/skill-modify/skill-modify.component';
import {LanguageCreateComponent} from './component/language/language-create/language-create.component';
import {LanguageListComponent} from './component/language/language-list/language-list.component';
import {LanguageModifyComponent} from './component/language/language-modify/language-modify.component';

const routes: Routes = [
    {path: '', component: WelcomeComponent},
    {path: 'log-in', component: LogInComponent, canActivate: [UnauthorizedGuard]},
    {path: 'profile-list', component: ProfileListComponent, canActivate: [AdminGuard]},
    {path: 'profile-modify/:profile-id', component: ProfileModifyComponent, canActivate: [IdGuard]},
    {path: 'language-list', component: LanguageListComponent, canActivate: [AdminGuard]},
    {path: 'language-create', component: LanguageCreateComponent, canActivate: [AdminGuard]},
    {path: 'language-modify/:language-id', component: LanguageModifyComponent, canActivate: [AdminGuard]},
    {path: 'skill-list', component: SkillListComponent, canActivate: [AdminGuard]},
    {path: 'skill-create', component: SkillCreateComponent, canActivate: [AdminGuard]},
    {path: 'skill-modify/:skill-id', component: SkillModifyComponent, canActivate: [AdminGuard]},
    {path: 'institution-list', component: InstitutionListComponent, canActivate: [AdminGuard]},
    {path: 'institution-create', component: InstitutionCreateComponent, canActivate: [AdminGuard]},
    {path: 'institution-modify/:institution-id', component: InstitutionModifyComponent, canActivate: [AdminGuard]},
    {path: 'specialty-list', component: SpecialtyListComponent, canActivate: [AdminGuard]},
    {path: 'specialty-create', component: SpecialtyCreateComponent, canActivate: [AdminGuard]},
    {path: 'specialty-modify/:specialty-id', component: SpecialtyModifyComponent, canActivate: [AdminGuard]},
    {path: 'company-list', component: CompanyListComponent, canActivate: [AdminGuard]},
    {path: 'company-create', component: CompanyCreateComponent, canActivate: [AdminGuard]},
    {path: 'company-modify/:company-id', component: CompanyModifyComponent, canActivate: [AdminGuard]},
    {path: 'position-list', component: PositionListComponent, canActivate: [AdminGuard]},
    {path: 'position-create', component: PositionCreateComponent, canActivate: [AdminGuard]},
    {path: 'position-modify/:position-id', component: PositionModifyComponent, canActivate: [AdminGuard]},
    {path: 'user-list', component: UserListComponent, canActivate: [AdminGuard]},
    {path: 'user-create', component: UserCreateComponent, canActivate: [AdminGuard]},
    {path: 'user-modify/:user-id', component: UserModifyComponent, canActivate: [AdminGuard]},
    {path: 'access-denied', component: AccessDeniedComponent},
    {path: '**', component: PageNotFoundComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
