import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from '../../../service/profile/profile.service';
import {SecurityService} from '../../../service/security/security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Position} from '../../../model/position/position';

@Component({
    selector: 'app-profile-experience-position-set',
    templateUrl: './profile-experience-position-set.component.html',
    styleUrls: ['./profile-experience-position-set.component.css']
})
export class ProfileExperiencePositionSetComponent implements OnInit {

    constructor(private profileService: ProfileService, private security: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    positions: Position[] = [];
    position = new FormControl('', [
        Validators.required
    ]);
    form = new FormGroup({
        position: this.position
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.positions = this.data.positions;
        if (this.data.positionId === 'none') {
            this.position.setValue(this.positions.find(position => position.id === 'none'));
        } else {
            this.position.setValue(this.positions.find(position => position.id === this.data.positionId));
        }
    }

    set(): void {
        if ((this.position.value as Position).id === 'none') {
            this.profileService.unsetExperiencePosition(this.data.profileId, this.data.experienceId).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        } else {
            this.profileService.setExperiencePosition(this.data.profileId, this.data.experienceId,
                (this.position.value as Position).id).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        }

    }

}

export class DialogData {

    constructor(public profileId: string, public experienceId: string, public positionId: string, public positions: Position[]) {
    }

}
