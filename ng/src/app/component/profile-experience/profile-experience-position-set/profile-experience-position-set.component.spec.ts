import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileExperiencePositionSetComponent } from './profile-experience-position-set.component';

describe('ProfileExperiencePositionSetComponent', () => {
  let component: ProfileExperiencePositionSetComponent;
  let fixture: ComponentFixture<ProfileExperiencePositionSetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileExperiencePositionSetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileExperiencePositionSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
