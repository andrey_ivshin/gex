import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileExperienceCompanySetComponent } from './profile-experience-company-set.component';

describe('ProfileExperienceCompanySetComponent', () => {
  let component: ProfileExperienceCompanySetComponent;
  let fixture: ComponentFixture<ProfileExperienceCompanySetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileExperienceCompanySetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileExperienceCompanySetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
