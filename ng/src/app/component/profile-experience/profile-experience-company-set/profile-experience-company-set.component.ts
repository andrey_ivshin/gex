import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from '../../../service/profile/profile.service';
import {SecurityService} from '../../../service/security/security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {Company} from '../../../model/company/company';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-profile-experience-company-set',
  templateUrl: './profile-experience-company-set.component.html',
  styleUrls: ['./profile-experience-company-set.component.css']
})
export class ProfileExperienceCompanySetComponent implements OnInit {

    constructor(private profileService: ProfileService, private security: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    companies: Company[] = [];
    company = new FormControl('', [
        Validators.required
    ]);
    form = new FormGroup({
        company: this.company
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.companies = this.data.companies;
        if (this.data.companyId === 'none') {
            this.company.setValue(this.companies.find(company => company.id === 'none'));
        } else {
            this.company.setValue(this.companies.find(company => company.id === this.data.companyId));
        }
    }

    set(): void {
        if ((this.company.value as Company).id === 'none') {
            this.profileService.unsetExperienceCompany(this.data.profileId, this.data.experienceId).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        } else {
            this.profileService.setExperienceCompany(this.data.profileId, this.data.experienceId,
                (this.company.value as Company).id).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        }

    }

}

export class DialogData {

    constructor(public profileId: string, public experienceId: string, public companyId: string, public companies: Company[]) {
    }

}
