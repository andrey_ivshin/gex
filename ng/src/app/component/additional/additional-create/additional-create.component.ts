import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from "../../../service/profile/profile.service";
import {SecurityService} from "../../../service/security/security.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ProfileModifyComponent} from "../../profile/profile-modify/profile-modify.component";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PointCreationTransfer} from "../../../model/point-creation-transfer/point-creation-transfer";
import {AdditionalCreationTransfer} from "../../../model/additional-cration-transfer/additional-creation-transfer";

@Component({
    selector: 'app-additional-create',
    templateUrl: './additional-create.component.html',
    styleUrls: ['./additional-create.component.css']
})
export class AdditionalCreateComponent implements OnInit {

    constructor(private profileService: ProfileService, private security: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
    }

    create(): void {
        this.profileService.addAdditional(this.data.profileId, new AdditionalCreationTransfer(this.name.value)).subscribe(
            profile => this.ref.close(profile),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}

export class DialogData {

    constructor(public profileId: string) {
    }

}
