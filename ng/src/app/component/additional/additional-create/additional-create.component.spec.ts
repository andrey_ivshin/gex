import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalCreateComponent } from './additional-create.component';

describe('AdditionalCreateComponent', () => {
  let component: AdditionalCreateComponent;
  let fixture: ComponentFixture<AdditionalCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdditionalCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
