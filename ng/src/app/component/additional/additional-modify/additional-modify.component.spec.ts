import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalModifyComponent } from './additional-modify.component';

describe('AdditionalModifyComponent', () => {
  let component: AdditionalModifyComponent;
  let fixture: ComponentFixture<AdditionalModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdditionalModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
