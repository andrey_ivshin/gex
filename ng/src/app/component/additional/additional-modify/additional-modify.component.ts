import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from '../../../service/profile/profile.service';
import {SecurityService} from '../../../service/security/security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AdditionalModificationTransfer} from '../../../model/additional-modification-transfer/additional-modification-transfer';

@Component({
    selector: 'app-additional-modify',
    templateUrl: './additional-modify.component.html',
    styleUrls: ['./additional-modify.component.css']
})
export class AdditionalModifyComponent implements OnInit {

    constructor(private profileService: ProfileService, private security: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.name.setValue(this.data.name);
    }

    modify(): void {
        this.profileService.modifyAdditional(this.data.profileId,
            new AdditionalModificationTransfer(this.data.additionalId, this.name.value)).subscribe(
            profile => this.ref.close(profile),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}

export class DialogData {

    constructor(public profileId: string, public additionalId: string, public name: string) {
    }

}
