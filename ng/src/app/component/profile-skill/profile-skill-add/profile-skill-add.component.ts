import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from '../../../service/profile/profile.service';
import {SecurityService} from '../../../service/security/security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {SkillService} from '../../../service/skill/skill.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SkillLevel} from '../../../model/skill-level/skill-level';
import {Skill} from '../../../model/skill/skill';
import {SkillLevelTransfer} from '../../../model/skill-level-transfer/skill-level-transfer';

@Component({
    selector: 'app-profile-skill-set',
    templateUrl: './profile-skill-add.component.html',
    styleUrls: ['./profile-skill-add.component.css']
})
export class ProfileSkillAddComponent implements OnInit {

    constructor(private profileService: ProfileService, private skillService: SkillService, private security: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    skills: Skill[] = [];
    levels = SkillLevel.values;
    skill = new FormControl('', [
        Validators.required
    ]);
    level = new FormControl('', [
        Validators.required
    ]);
    form = new FormGroup({
        skill: this.skill,
        level: this.level
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.skills = this.data.skills;
    }

    add(): void {
        this.profileService.setProfileSkill(this.data.profileId, (this.skill.value as Skill).id,
            new SkillLevelTransfer(this.level.value)).subscribe(
            profile => this.ref.close(profile),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}

export class DialogData {

    constructor(public profileId: string, public skills: Skill[]) {
    }

}
