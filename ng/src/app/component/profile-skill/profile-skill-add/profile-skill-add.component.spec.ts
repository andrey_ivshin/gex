import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileSkillAddComponent } from './profile-skill-add.component';

describe('ProfileSkillSetComponent', () => {
  let component: ProfileSkillAddComponent;
  let fixture: ComponentFixture<ProfileSkillAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileSkillAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileSkillAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
