import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from '../../../service/profile/profile.service';
import {SkillService} from '../../../service/skill/skill.service';
import {SecurityService} from '../../../service/security/security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {Skill} from '../../../model/skill/skill';
import {SkillLevel} from '../../../model/skill-level/skill-level';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SkillLevelTransfer} from '../../../model/skill-level-transfer/skill-level-transfer';

@Component({
    selector: 'app-profile-skill-modify',
    templateUrl: './profile-skill-modify.component.html',
    styleUrls: ['./profile-skill-modify.component.css']
})
export class ProfileSkillModifyComponent implements OnInit {

    constructor(private profileService: ProfileService, private skillService: SkillService, private security: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    levels = SkillLevel.values;
    skill: Skill = new Skill('', '');
    level = new FormControl('', [
        Validators.required
    ]);
    form = new FormGroup({
        level: this.level
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.skill = this.data.skill;
        this.levels.forEach(level => {
            if (this.data.level === level) {
                this.level.setValue(level);
            }
        });
    }

    modify(): void {
        this.profileService.setProfileSkill(this.data.profileId, this.data.skill.id,
            new SkillLevelTransfer(this.level.value)).subscribe(
            profile => this.ref.close(profile),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}

export class DialogData {

    constructor(public profileId: string, public skill: Skill, public level: string) {
    }

}
