import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileSkillModifyComponent } from './profile-skill-modify.component';

describe('ProfileSkillModifyComponent', () => {
  let component: ProfileSkillModifyComponent;
  let fixture: ComponentFixture<ProfileSkillModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileSkillModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileSkillModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
