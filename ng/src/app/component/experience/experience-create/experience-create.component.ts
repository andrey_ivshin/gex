import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {ProfileService} from '../../../service/profile/profile.service';
import {SecurityService} from '../../../service/security/security.service';
import {ExperienceCreationTransfer} from '../../../model/experience-creation-transfer/experience-creation-transfer';

@Component({
    selector: 'app-experience-create',
    templateUrl: './experience-create.component.html',
    styleUrls: ['./experience-create.component.css']
})
export class ExperienceCreateComponent implements OnInit {

    constructor(private profileService: ProfileService, private securityService: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    start = new FormControl('', [
        Validators.required
    ]);
    stop = new FormControl('', []);
    company = new FormControl('', []);
    position = new FormControl('', []);
    form = new FormGroup({
        start: this.start,
        stop: this.stop
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
    }

    create(): void {
        this.profileService.addExperience(this.data.profileId,
            new ExperienceCreationTransfer(this.start.value, this.stop.value !== '' ? this.stop.value : null)).subscribe(
            profile => this.ref.close(profile),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.securityService.getErrorMessage(err);
            }
        );
    }

}

export class DialogData {

    constructor(public profileId: string) {
    }

}
