import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from '../../../service/profile/profile.service';
import {SecurityService} from '../../../service/security/security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ExperienceModificationTransfer} from '../../../model/experience-modification-transfer/experience-modification-transfer';

@Component({
    selector: 'app-experience-modify',
    templateUrl: './experience-modify.component.html',
    styleUrls: ['./experience-modify.component.css']
})
export class ExperienceModifyComponent implements OnInit {

    constructor(private profileService: ProfileService, private securityService: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    start = new FormControl('', [
        Validators.required
    ]);
    stop = new FormControl('', []);
    company = new FormControl('', []);
    position = new FormControl('', []);
    form = new FormGroup({
        start: this.start,
        stop: this.stop
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.start.setValue(this.data.start);
        this.stop.setValue(this.data.stop == null ? this.data.stop : '');
    }

    modify(): void {
        this.profileService.modifyExperience(this.data.profileId,
            new ExperienceModificationTransfer(this.data.experienceId, this.start.value,
                this.stop.value !== '' ? this.stop.value : null)).subscribe(
            profile => this.ref.close(profile),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.securityService.getErrorMessage(err);
            }
        );
    }

}

export class DialogData {

    constructor(public profileId: string, public experienceId: string, public start: Date, public stop: Date) {
    }

}
