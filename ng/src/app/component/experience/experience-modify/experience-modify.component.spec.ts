import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperienceModifyComponent } from './experience-modify.component';

describe('ExperienceModifyComponent', () => {
  let component: ExperienceModifyComponent;
  let fixture: ComponentFixture<ExperienceModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExperienceModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienceModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
