import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PositionService} from '../../../service/position/position.service';
import {PositionCreationTransfer} from '../../../model/position-creation-transfer/position-creation-transfer';

@Component({
    selector: 'app-position-create',
    templateUrl: './position-create.component.html',
    styleUrls: ['./position-create.component.css']
})
export class PositionCreateComponent implements OnInit {

    constructor(private positionService: PositionService, private router: Router, private security: SecurityService) {
    }

    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
    }

    create(): void {
        this.positionService.createPosition(new PositionCreationTransfer(this.name.value)).subscribe(
            () => this.router.navigate([`/position-list`]),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}
