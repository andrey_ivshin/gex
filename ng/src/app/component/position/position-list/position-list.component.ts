import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {MatDialog} from '@angular/material/dialog';
import {AreYouSureComponent} from '../../.stereotype/are-you-sure/are-you-sure.component';
import {PositionService} from '../../../service/position/position.service';
import {Position} from '../../../model/position/position';

@Component({
  selector: 'app-position-list',
  templateUrl: './position-list.component.html',
  styleUrls: ['./position-list.component.css']
})
export class PositionListComponent implements OnInit {

    constructor(private positionService: PositionService, private router:
        Router, private security: SecurityService, private dialog: MatDialog) {
    }

    ready = false;
    positions: Position[] = [];

    ngOnInit(): void {
        this.positionService.getPositions().subscribe(
            positions => {
                this.positions = positions;
                this.ready = true;
            },
            err => {
                alert(this.security.getErrorMessage(err));
            });
    }

    remove(id: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.positionService.removePosition(id).subscribe(
                    () => window.location.reload(),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

}
