import {Component, OnInit} from '@angular/core';
import {PositionService} from '../../../service/position/position.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {PositionModificationTransfer} from '../../../model/position-modification-transfer/position-modification-transfer';

@Component({
    selector: 'app-position-modify',
    templateUrl: './position-modify.component.html',
    styleUrls: ['./position-modify.component.css']
})
export class PositionModifyComponent implements OnInit {

    constructor(private positionService: PositionService, private router: Router,
                private security: SecurityService, private activatedRoute: ActivatedRoute) {
    }

    id = 'none';
    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(value => this.id = value.get('position-id') || 'none');
        this.positionService.getPosition(this.id).subscribe(
            position => this.name.setValue(position.name),
            err => alert(this.security.getErrorMessage(err)));
    }

    modify(): void {
        this.positionService.modifyPosition(new PositionModificationTransfer(this.id, this.name.value)).subscribe(
            () => this.router.navigate([`/position-list`]),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}
