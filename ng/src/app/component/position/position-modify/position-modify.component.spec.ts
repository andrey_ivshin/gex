import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PositionModifyComponent} from './position-modify.component';

describe('PositionModifyComponent', () => {
  let component: PositionModifyComponent;
  let fixture: ComponentFixture<PositionModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
