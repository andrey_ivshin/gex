import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../service/user/user.service';
import {Router} from '@angular/router';
import {User} from '../../../model/user/user';
import {SecurityService} from '../../../service/security/security.service';
import {MatDialog} from '@angular/material/dialog';
import {AreYouSureComponent} from '../../.stereotype/are-you-sure/are-you-sure.component';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

    constructor(private userService: UserService, private router: Router,
                private security: SecurityService, private dialog: MatDialog) {
    }

    ready = false;
    users: User[] = [];

    ngOnInit(): void {
        this.userService.getUsers().subscribe(
            users => {
                this.users = users;
                this.ready = true;
            },
            err => alert(this.security.getErrorMessage(err)));
    }

    remove(id: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.userService.removeUser(id).subscribe(
                    () => window.location.reload(),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

}
