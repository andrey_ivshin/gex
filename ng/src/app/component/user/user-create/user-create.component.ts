import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../service/user/user.service';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {UserCreationRequest} from '../../../model/user-creation-request/user-creation-request';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-user-create',
    templateUrl: './user-create.component.html',
    styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

    constructor(private userService: UserService, private router: Router, private security: SecurityService) {
    }

    ready = false;
    roles = ['USER', 'ADMIN'];
    serverError = false;
    serverErrorMessage = '';
    email = new FormControl('', [
        Validators.required,
        Validators.email
    ]);
    password = new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50)
    ]);
    passwordConfirmation = new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50)
    ]);
    role = new FormControl('', [
        Validators.required,
    ]);
    form = new FormGroup({
        email: this.email,
        password: this.password,
        passwordRepeat: this.passwordConfirmation,
        role: this.role
    });

    ngOnInit(): void {
    }

    create(): void {
        this.userService.createUser(
            new UserCreationRequest(
                this.email.value,
                this.password.value,
                this.passwordConfirmation.value,
                this.role.value))
            .subscribe(
                () => this.router.navigate([`/user-list`]),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
    }

    passwordConfirmed(): boolean {
        return this.password.value === this.passwordConfirmation.value;
    }

}
