import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../service/user/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserModificationRequest} from '../../../model/user-modification-request/user-modification-request';
import {UserRole} from '../../../model/user-role/user-role';

@Component({
    selector: 'app-user-modify',
    templateUrl: './user-modify.component.html',
    styleUrls: ['./user-modify.component.css']
})
export class UserModifyComponent implements OnInit {

    constructor(private userService: UserService, private router: Router,
                private activatedRoute: ActivatedRoute, private security: SecurityService) {
    }

    ready = false;
    roles = UserRole.values;
    serverError = false;
    serverErrorMessage = '';
    id = 'none';
    email = new FormControl('', [
        Validators.required,
        Validators.email
    ]);
    password = new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50)
    ]);
    passwordConfirmation = new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50)
    ]);
    role = new FormControl('', [
        Validators.required,
    ]);
    form = new FormGroup({
        email: this.email,
        password: this.password,
        passwordRepeat: this.passwordConfirmation,
        role: this.role
    });

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(value => this.id = value.get('user-id') || 'none');
        this.userService.getUser(this.id).subscribe(
            user => {
                this.email.setValue(user.email);
                this.role.setValue(user.role);
            }, err => alert(this.security.getErrorMessage(err)));
    }

    modify(): void {
        this.userService.modifyUser(
            new UserModificationRequest(
                this.id,
                this.email.value,
                this.password.value,
                this.passwordConfirmation.value,
                this.role.value))
            .subscribe(
                () => this.router.navigate([`/user-list`]),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
    }

    passwordConfirmed(): boolean {
        return this.password.value === this.passwordConfirmation.value;
    }


}
