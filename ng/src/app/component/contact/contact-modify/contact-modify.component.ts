import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from '../../../service/profile/profile.service';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {ContactType} from '../../../model/contact-type/contact-type';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ContactModificationTransfer} from '../../../model/contact-modification-transfer/contact-modification-transfer';
import {Contact} from '../../../model/contact/contact';

@Component({
  selector: 'app-contact-modify',
  templateUrl: './contact-modify.component.html',
  styleUrls: ['./contact-modify.component.css']
})
export class ContactModifyComponent implements OnInit {

    constructor(private profileService: ProfileService, private security: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    types = ContactType.values;
    value = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    type = new FormControl('', [
        Validators.required
    ]);
    form = new FormGroup({
        name: this.value,
        type: this.type
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.value.setValue(this.data.contact.value);
        this.type.setValue(this.data.contact.type);
    }

    modify(): void {
        this.profileService.modifyContact(this.data.profileId,
            new ContactModificationTransfer(this.data.contact.id, this.value.value, this.type.value)).subscribe(
            profile => {
                this.ref.close(profile);
            },
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}

export class DialogData {

    constructor(public profileId: string, public contact: Contact) {
    }

}
