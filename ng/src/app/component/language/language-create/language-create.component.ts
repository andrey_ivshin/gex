import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../../../service/language/language.service';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LanguageCreationTransfer} from '../../../model/language-creation-transfer/language-creation-transfer';

@Component({
  selector: 'app-language-create',
  templateUrl: './language-create.component.html',
  styleUrls: ['./language-create.component.css']
})
export class LanguageCreateComponent implements OnInit {

    constructor(private languageService: LanguageService, private router: Router, private security: SecurityService) {
    }

    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
    }

    create(): void {
        this.languageService.createLanguage(new LanguageCreationTransfer(this.name.value)).subscribe(
            () => this.router.navigate([`/language-list`]),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}
