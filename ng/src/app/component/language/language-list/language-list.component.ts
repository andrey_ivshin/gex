import {Component, OnInit} from '@angular/core';
import {LanguageService} from "../../../service/language/language.service";
import {Router} from "@angular/router";
import {SecurityService} from "../../../service/security/security.service";
import {MatDialog} from "@angular/material/dialog";
import {Language} from "../../../model/language/language";
import {AreYouSureComponent} from "../../.stereotype/are-you-sure/are-you-sure.component";

@Component({
  selector: 'app-language-list',
  templateUrl: './language-list.component.html',
  styleUrls: ['./language-list.component.css']
})
export class LanguageListComponent implements OnInit {

    constructor(private languageService: LanguageService, private router:
        Router, private security: SecurityService, private dialog: MatDialog) {
    }

    ready = false;
    languages: Language[] = [];

    ngOnInit(): void {
        this.languageService.getLanguages().subscribe(
            languages => {
                this.languages = languages;
                this.ready = true;
            },
            err => {
                alert(this.security.getErrorMessage(err));
            });
    }

    remove(id: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.languageService.removeLanguage(id).subscribe(
                    () => window.location.reload(),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

}
