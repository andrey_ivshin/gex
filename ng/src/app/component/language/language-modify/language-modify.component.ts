import {Component, OnInit} from '@angular/core';
import {LanguageService} from "../../../service/language/language.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SecurityService} from "../../../service/security/security.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LanguageModificationTransfer} from "../../../model/language-modification-transfer/language-modification-transfer";

@Component({
  selector: 'app-language-modify',
  templateUrl: './language-modify.component.html',
  styleUrls: ['./language-modify.component.css']
})
export class LanguageModifyComponent implements OnInit {

    constructor(private languageService: LanguageService, private router: Router,
                private security: SecurityService, private activatedRoute: ActivatedRoute) {
    }

    id = 'none';
    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(value => this.id = value.get('language-id') || 'none');
        this.languageService.getLanguage(this.id).subscribe(
            language => this.name.setValue(language.name),
            err => alert(this.security.getErrorMessage(err)));
    }

    modify(): void {
        this.languageService.modifyLanguage(new LanguageModificationTransfer(this.id, this.name.value)).subscribe(
            () => this.router.navigate([`/language-list`]),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }


}
