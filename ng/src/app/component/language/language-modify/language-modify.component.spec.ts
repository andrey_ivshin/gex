import {ComponentFixture, TestBed} from '@angular/core/testing';

import {LanguageModifyComponent} from './language-modify.component';

describe('LanguageModifyComponent', () => {
  let component: LanguageModifyComponent;
  let fixture: ComponentFixture<LanguageModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LanguageModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
