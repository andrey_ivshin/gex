import {Component, OnInit} from '@angular/core';
import {InstitutionService} from '../../../service/institution/institution.service';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {InstitutionCreationTransfer} from '../../../model/institution-creation-transfer/institution-creation-transfer';

@Component({
  selector: 'app-institution-create',
  templateUrl: './institution-create.component.html',
  styleUrls: ['./institution-create.component.css']
})
export class InstitutionCreateComponent implements OnInit {

    constructor(private institutionService: InstitutionService, private router: Router, private security: SecurityService) {
    }

    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
    }

    create(): void {
        this.institutionService.createInstitution(new InstitutionCreationTransfer(this.name.value)).subscribe(
            () => this.router.navigate([`/institution-list`]),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}
