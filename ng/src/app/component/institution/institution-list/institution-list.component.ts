import {Component, OnInit} from '@angular/core';
import {InstitutionService} from '../../../service/institution/institution.service';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {MatDialog} from '@angular/material/dialog';
import {Institution} from '../../../model/institution/institution';
import {AreYouSureComponent} from '../../.stereotype/are-you-sure/are-you-sure.component';

@Component({
    selector: 'app-institution-list',
    templateUrl: './institution-list.component.html',
    styleUrls: ['./institution-list.component.css']
})
export class InstitutionListComponent implements OnInit {

    constructor(private institutionService: InstitutionService, private router:
        Router, private security: SecurityService, private dialog: MatDialog) {
    }

    ready = false;
    institutions: Institution[] = [];

    ngOnInit(): void {
        this.institutionService.getInstitutions().subscribe(
            institutions => {
                this.institutions = institutions;
                this.ready = true;
            },
            err => {
                alert(this.security.getErrorMessage(err));
            });
    }

    remove(id: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.institutionService.removeInstitution(id).subscribe(
                    () => window.location.reload(),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

}
