import {Component, OnInit} from '@angular/core';
import {InstitutionService} from '../../../service/institution/institution.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {InstitutionModificationTransfer} from '../../../model/institution-modification-transfer/institution-modification-transfer';

@Component({
  selector: 'app-institution-modify',
  templateUrl: './institution-modify.component.html',
  styleUrls: ['./institution-modify.component.css']
})
export class InstitutionModifyComponent implements OnInit {

    constructor(private institutionService: InstitutionService, private router: Router,
                private security: SecurityService, private activatedRoute: ActivatedRoute) {
    }

    id = 'none';
    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(value => this.id = value.get('institution-id') || 'none');
        this.institutionService.getInstitution(this.id).subscribe(
            institution => this.name.setValue(institution.name),
            err => alert(this.security.getErrorMessage(err)));
    }

    modify(): void {
        this.institutionService.modifyInstitution(new InstitutionModificationTransfer(this.id, this.name.value)).subscribe(
            () => this.router.navigate([`/institution-list`]),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}
