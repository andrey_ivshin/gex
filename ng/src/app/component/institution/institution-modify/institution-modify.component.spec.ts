import {ComponentFixture, TestBed} from '@angular/core/testing';

import {InstitutionModifyComponent} from './institution-modify.component';

describe('InstitutionModifyComponent', () => {
  let component: InstitutionModifyComponent;
  let fixture: ComponentFixture<InstitutionModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstitutionModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutionModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
