import {Component, OnInit} from '@angular/core';
import {CompanyService} from '../../../service/company/company.service';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {MatDialog} from '@angular/material/dialog';
import {Company} from '../../../model/company/company';
import {AreYouSureComponent} from '../../.stereotype/are-you-sure/are-you-sure.component';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {

    constructor(private companyService: CompanyService, private router:
        Router, private security: SecurityService, private dialog: MatDialog) {
    }

    ready = false;
    companies: Company[] = [];

    ngOnInit(): void {
        this.companyService.getCompanies().subscribe(
            companies => {
                this.companies = companies;
                this.ready = true;
            },
            err => {
                alert(this.security.getErrorMessage(err));
            });
    }

    remove(id: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.companyService.removeCompany(id).subscribe(
                    () => window.location.reload(),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

}
