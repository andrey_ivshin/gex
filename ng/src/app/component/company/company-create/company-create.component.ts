import {Component, OnInit} from '@angular/core';
import {CompanyService} from '../../../service/company/company.service';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CompanyCreationTransfer} from '../../../model/company-creation-transfer/company-creation-transfer';

@Component({
  selector: 'app-company-create',
  templateUrl: './company-create.component.html',
  styleUrls: ['./company-create.component.css']
})
export class CompanyCreateComponent implements OnInit {

    constructor(private companyService: CompanyService, private router: Router, private security: SecurityService) {
    }

    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
    }

    create(): void {
        this.companyService.createCompany(new CompanyCreationTransfer(this.name.value)).subscribe(
            () => this.router.navigate([`/company-list`]),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}
