import {Component, OnInit} from '@angular/core';
import {CompanyService} from '../../../service/company/company.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CompanyModificationTransfer} from '../../../model/company-modification-transfer/company-modification-transfer';

@Component({
  selector: 'app-company-modify',
  templateUrl: './company-modify.component.html',
  styleUrls: ['./company-modify.component.css']
})
export class CompanyModifyComponent implements OnInit {

    constructor(private companyService: CompanyService, private router: Router,
                private security: SecurityService, private activatedRoute: ActivatedRoute) {
    }

    id = 'none';
    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(value => this.id = value.get('company-id') || 'none');
        this.companyService.getCompany(this.id).subscribe(
            company => this.name.setValue(company.name),
            err => alert(this.security.getErrorMessage(err)));
    }

    modify(): void {
        this.companyService.modifyCompany(new CompanyModificationTransfer(this.id, this.name.value)).subscribe(
            () => this.router.navigate([`/company-list`]),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}
