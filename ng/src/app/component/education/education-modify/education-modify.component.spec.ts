import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationModifyComponent } from './education-modify.component';

describe('EducationModifyComponent', () => {
  let component: EducationModifyComponent;
  let fixture: ComponentFixture<EducationModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EducationModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
