import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from '../../../service/profile/profile.service';
import {SecurityService} from '../../../service/security/security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EducationCreationTransfer} from '../../../model/education-creation-transfer/education-creation-transfer';

@Component({
  selector: 'app-education-create',
  templateUrl: './education-create.component.html',
  styleUrls: ['./education-create.component.css']
})
export class EducationCreateComponent implements OnInit {

    constructor(private profileService: ProfileService, private securityService: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    start = new FormControl('', [
        Validators.required
    ]);
    stop = new FormControl('', []);
    company = new FormControl('', []);
    position = new FormControl('', []);
    form = new FormGroup({
        start: this.start,
        stop: this.stop
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
    }

    create(): void {
        this.profileService.addEducation(this.data.profileId,
            new EducationCreationTransfer(this.start.value, this.stop.value !== '' ? this.stop.value : null)).subscribe(
            profile => this.ref.close(profile),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.securityService.getErrorMessage(err);
            }
        );
    }

}

export class DialogData {

    constructor(public profileId: string) {
    }

}
