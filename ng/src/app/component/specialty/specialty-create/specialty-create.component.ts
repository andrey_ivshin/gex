import {Component, OnInit} from '@angular/core';
import {SpecialtyService} from '../../../service/specialty/specialty.service';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SpecialtyCreationTransfer} from '../../../model/specialty-creation-transfer/specialty-creation-transfer';

@Component({
  selector: 'app-specialty-create',
  templateUrl: './specialty-create.component.html',
  styleUrls: ['./specialty-create.component.css']
})
export class SpecialtyCreateComponent implements OnInit {

    constructor(private specialtyService: SpecialtyService, private router: Router, private security: SecurityService) {
    }

    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
    }

    create(): void {
        this.specialtyService.createSpecialty(new SpecialtyCreationTransfer(this.name.value)).subscribe(
            () => this.router.navigate([`/specialty-list`]),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}
