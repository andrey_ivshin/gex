import {Component, OnInit} from '@angular/core';
import {SpecialtyService} from '../../../service/specialty/specialty.service';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {MatDialog} from '@angular/material/dialog';
import {Specialty} from '../../../model/specialty/specialty';
import {AreYouSureComponent} from '../../.stereotype/are-you-sure/are-you-sure.component';

@Component({
  selector: 'app-specialty-list',
  templateUrl: './specialty-list.component.html',
  styleUrls: ['./specialty-list.component.css']
})
export class SpecialtyListComponent implements OnInit {

    constructor(private specialtyService: SpecialtyService, private router:
        Router, private security: SecurityService, private dialog: MatDialog) {
    }

    ready = false;
    specialties: Specialty[] = [];

    ngOnInit(): void {
        this.specialtyService.getSpecialties().subscribe(
            specialties => {
                this.specialties = specialties;
                this.ready = true;
            },
            err => {
                alert(this.security.getErrorMessage(err));
            });
    }

    remove(id: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.specialtyService.removeSpecialty(id).subscribe(
                    () => window.location.reload(),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

}
