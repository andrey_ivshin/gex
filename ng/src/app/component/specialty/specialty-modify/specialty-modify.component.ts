import {Component, OnInit} from '@angular/core';
import {SpecialtyService} from '../../../service/specialty/specialty.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SpecialtyModificationTransfer} from '../../../model/specialty-modification-transfer/specialty-modification-transfer';

@Component({
  selector: 'app-specialty-modify',
  templateUrl: './specialty-modify.component.html',
  styleUrls: ['./specialty-modify.component.css']
})
export class SpecialtyModifyComponent implements OnInit {

    constructor(private specialtyService: SpecialtyService, private router: Router,
                private security: SecurityService, private activatedRoute: ActivatedRoute) {
    }

    id = 'none';
    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(value => this.id = value.get('specialty-id') || 'none');
        this.specialtyService.getSpecialty(this.id).subscribe(
            specialty => this.name.setValue(specialty.name),
            err => alert(this.security.getErrorMessage(err)));
    }

    modify(): void {
        this.specialtyService.modifySpecialty(new SpecialtyModificationTransfer(this.id, this.name.value)).subscribe(
            () => this.router.navigate([`/specialty-list`]),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}
