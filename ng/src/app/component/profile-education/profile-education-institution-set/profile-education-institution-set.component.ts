import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from '../../../service/profile/profile.service';
import {SecurityService} from '../../../service/security/security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {Institution} from '../../../model/institution/institution';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-profile-education-institution-set',
  templateUrl: './profile-education-institution-set.component.html',
  styleUrls: ['./profile-education-institution-set.component.css']
})
export class ProfileEducationInstitutionSetComponent implements OnInit {

    constructor(private profileService: ProfileService, private security: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    institutions: Institution[] = [];
    institution = new FormControl('', [
        Validators.required
    ]);
    form = new FormGroup({
        institution: this.institution
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.institutions = this.data.institutions;
        if (this.data.institutionId === 'none') {
            this.institution.setValue(this.institutions.find(institution => institution.id === 'none'));
        } else {
            this.institution.setValue(this.institutions.find(institution => institution.id === this.data.institutionId));
        }
    }

    set(): void {
        if ((this.institution.value as Institution).id === 'none') {
            this.profileService.unsetEducationInstitution(this.data.profileId, this.data.educationId).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        } else {
            this.profileService.setEducationInstitution(this.data.profileId, this.data.educationId,
                (this.institution.value as Institution).id).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        }

    }

}

export class DialogData {

    constructor(public profileId: string, public educationId: string, public institutionId: string, public institutions: Institution[]) {
    }

}
