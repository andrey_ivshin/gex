import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileEducationInstitutionSetComponent } from './profile-education-institution-set.component';

describe('ProfileEducationInstitutionSetComponent', () => {
  let component: ProfileEducationInstitutionSetComponent;
  let fixture: ComponentFixture<ProfileEducationInstitutionSetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileEducationInstitutionSetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileEducationInstitutionSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
