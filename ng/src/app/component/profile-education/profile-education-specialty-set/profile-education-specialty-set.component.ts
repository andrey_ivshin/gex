import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from '../../../service/profile/profile.service';
import {SecurityService} from '../../../service/security/security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {Specialty} from '../../../model/specialty/specialty';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-profile-education-specialty-set',
  templateUrl: './profile-education-specialty-set.component.html',
  styleUrls: ['./profile-education-specialty-set.component.css']
})
export class ProfileEducationSpecialtySetComponent implements OnInit {

    constructor(private profileService: ProfileService, private security: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    specialties: Specialty[] = [];
    specialty = new FormControl('', [
        Validators.required
    ]);
    form = new FormGroup({
        specialty: this.specialty
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.specialties = this.data.specialties;
        if (this.data.specialtyId === 'none') {
            this.specialty.setValue(this.specialties.find(specialty => specialty.id === 'none'));
        } else {
            this.specialty.setValue(this.specialties.find(specialty => specialty.id === this.data.specialtyId));
        }
    }

    set(): void {
        if ((this.specialty.value as Specialty).id === 'none') {
            this.profileService.unsetEducationSpecialty(this.data.profileId, this.data.educationId).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        } else {
            this.profileService.setEducationSpecialty(this.data.profileId, this.data.educationId,
                (this.specialty.value as Specialty).id).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        }

    }

}

export class DialogData {

    constructor(public profileId: string, public educationId: string, public specialtyId: string, public specialties: Specialty[]) {
    }

}
