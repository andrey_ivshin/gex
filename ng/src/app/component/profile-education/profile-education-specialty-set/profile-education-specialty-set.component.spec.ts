import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileEducationSpecialtySetComponent } from './profile-education-specialty-set.component';

describe('ProfileEducationSpecialtySetComponent', () => {
  let component: ProfileEducationSpecialtySetComponent;
  let fixture: ComponentFixture<ProfileEducationSpecialtySetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileEducationSpecialtySetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileEducationSpecialtySetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
