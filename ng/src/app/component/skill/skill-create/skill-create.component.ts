import {Component, OnInit} from '@angular/core';
import {SkillService} from '../../../service/skill/skill.service';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SkillCreationTransfer} from '../../../model/skill-creation-transfer/skill-creation-transfer';

@Component({
  selector: 'app-skill-create',
  templateUrl: './skill-create.component.html',
  styleUrls: ['./skill-create.component.css']
})
export class SkillCreateComponent implements OnInit {

    constructor(private skillService: SkillService, private router: Router, private security: SecurityService) {
    }

    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
    }

    create(): void {
        this.skillService.createSkill(new SkillCreationTransfer(this.name.value)).subscribe(
            () => this.router.navigate([`/skill-list`]),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}
