import {Component, OnInit} from '@angular/core';
import {SkillService} from '../../../service/skill/skill.service';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {MatDialog} from '@angular/material/dialog';
import {Skill} from '../../../model/skill/skill';
import {AreYouSureComponent} from '../../.stereotype/are-you-sure/are-you-sure.component';

@Component({
  selector: 'app-skill-list',
  templateUrl: './skill-list.component.html',
  styleUrls: ['./skill-list.component.css']
})
export class SkillListComponent implements OnInit {

    constructor(private skillService: SkillService, private router:
        Router, private security: SecurityService, private dialog: MatDialog) {
    }

    ready = false;
    skills: Skill[] = [];

    ngOnInit(): void {
        this.skillService.getSkills().subscribe(
            skills => {
                this.skills = skills;
                this.ready = true;
            },
            err => {
                alert(this.security.getErrorMessage(err));
            });
    }

    remove(id: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.skillService.removeSkill(id).subscribe(
                    () => window.location.reload(),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

}
