import {Component, OnInit} from '@angular/core';
import {SkillService} from '../../../service/skill/skill.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SkillModificationTransfer} from '../../../model/skill-modification-transfer/skill-modification-transfer';

@Component({
  selector: 'app-skill-modify',
  templateUrl: './skill-modify.component.html',
  styleUrls: ['./skill-modify.component.css']
})
export class SkillModifyComponent implements OnInit {

    constructor(private skillService: SkillService, private router: Router,
                private security: SecurityService, private activatedRoute: ActivatedRoute) {
    }

    id = 'none';
    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.name
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.activatedRoute.paramMap.subscribe(value => this.id = value.get('skill-id') || 'none');
        this.skillService.getSkill(this.id).subscribe(
            skill => this.name.setValue(skill.name),
            err => alert(this.security.getErrorMessage(err)));
    }

    modify(): void {
        this.skillService.modifySkill(new SkillModificationTransfer(this.id, this.name.value)).subscribe(
            () => this.router.navigate([`/skill-list`]),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}
