import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SkillModifyComponent} from './skill-modify.component';

describe('SkillModifyComponent', () => {
  let component: SkillModifyComponent;
  let fixture: ComponentFixture<SkillModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkillModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
