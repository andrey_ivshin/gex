import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {MatDialog} from '@angular/material/dialog';
import {AreYouSureComponent} from '../../.stereotype/are-you-sure/are-you-sure.component';
import {ProfileService} from '../../../service/profile/profile.service';
import {Profile} from '../../../model/profile/profile';
import {ProfileTransfer} from '../../../model/profile-transfer/profile-transfer';
import {User} from '../../../model/user/user';
import {ProfileCreateAdminComponent} from '../profile-create-admin/profile-create-admin.component';
import {Skill} from "../../../model/skill/skill";
import {Language} from "../../../model/language/language";
import {Position} from "../../../model/position/position";
import {Specialty} from "../../../model/specialty/specialty";
import {PositionService} from "../../../service/position/position.service";
import {SpecialtyService} from "../../../service/specialty/specialty.service";
import {SkillService} from "../../../service/skill/skill.service";
import {LanguageService} from "../../../service/language/language.service";
import {SkillLevel} from "../../../model/skill-level/skill-level";
import {LanguageLevel} from "../../../model/language-level/language-level";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {
    LanguageSearchCriteria,
    PositionSearchCriteria,
    ProfileSearchCriteria,
    SkillSearchCriteria,
    SpecialtySearchCriteria
} from "../../../model/profile-search-criteria/profile-search-criteria";

@Component({
    selector: 'app-profile-list',
    templateUrl: './profile-list.component.html',
    styleUrls: ['./profile-list.component.css']
})
export class ProfileListComponent implements OnInit {

    constructor(private profileService: ProfileService, private router: Router,
                private security: SecurityService, private dialog: MatDialog,
                private positionService: PositionService, private specialtyService: SpecialtyService,
                private skillService: SkillService, private languageService: LanguageService) {
    }

    positions: Position[] = [];
    selectedPositions: Position[] = [];
    position = new FormControl('', {validators: [Validators.required]});
    positionForm = new FormGroup({position: this.position});

    specialties: Specialty[] = [];
    selectedSpecialties: Specialty[] = [];
    specialty = new FormControl('', {validators: [Validators.required]});
    specialtyForm = new FormGroup({specialty: this.specialty});

    skills: Skill[] = [];
    skillLevels = SkillLevel.values;
    selectedSkills: SkillPair[] = [];
    skill = new FormControl('', {validators: [Validators.required]});
    skillLevel = new FormControl('', {validators: [Validators.required]});
    skillForm = new FormGroup({skill: this.skill, skillLevel: this.skillLevel});

    languages: Language[] = [];
    languageLevels = LanguageLevel.values;
    selectedLanguages: LanguagePair[] = [];
    language = new FormControl('', {validators: [Validators.required]});
    languageLevel = new FormControl('', {validators: [Validators.required]});
    languageForm = new FormGroup({language: this.language, languageLevel: this.languageLevel});

    ready = false;
    profiles: Profile[] = [];

    ngOnInit(): void {
        this.skillService.getSkills().subscribe(
            skills => this.skills = skills,
            err => alert(this.security.getErrorMessage(err)));

        this.languageService.getLanguages().subscribe(
            languages => this.languages = languages,
            err => alert(this.security.getErrorMessage(err)));

        this.positionService.getPositions().subscribe(
            positions => this.positions = positions,
            err => alert(this.security.getErrorMessage(err)));

        this.specialtyService.getSpecialties().subscribe(
            specialties => this.specialties = specialties,
            err => alert(this.security.getErrorMessage(err)));

        this.profileService.getProfiles().subscribe(
            profiles => {
                this.profiles = profiles;
                this.ready = true;
            },
            err => {
                alert(this.security.getErrorMessage(err));
            });
    }

    remove(id: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.profileService.removeProfile(id).subscribe(
                    () => window.location.reload(),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

    create(): void {
        const ref = this.dialog.open(ProfileCreateAdminComponent);
        ref.afterClosed().subscribe(user => {
            if (user !== null) {
                this.profileService.createProfile(new ProfileTransfer(
                    (user as User).id, 'Name', 'About'
                )).subscribe(
                    () => window.location.reload(),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

    addPosition(): void {
        if (this.selectedPositions.indexOf(this.position.value) === -1) {
            this.selectedPositions.push(this.position.value);
        }
    }

    removePosition(position: Position): void {
        const tmp: Position[] = [];
        for (const pos of this.selectedPositions) {
            if (pos !== position) {
                tmp.push(pos);
            }
        }
        this.selectedPositions = tmp;
    }

    addSpecialty(): void {
        if (this.selectedSpecialties.indexOf(this.specialty.value) === -1) {
            this.selectedSpecialties.push(this.specialty.value);
        }
    }

    removeSpecialty(specialty: Specialty): void {
        const tmp: Specialty[] = [];
        for (const spc of this.selectedSpecialties) {
            if (spc !== specialty) {
                tmp.push(spc);
            }
        }
        this.selectedSpecialties = tmp;
    }

    addSkill(): void {
        if (this.selectedSkills.every(value => value.skill !== this.skill.value)) {
            this.selectedSkills.push(new SkillPair(this.skill.value, this.skillLevel.value));
        }
    }

    removeSkill(skillPair: SkillPair): void {
        const tmp: SkillPair[] = [];
        for (const skl of this.selectedSkills) {
            if (skl !== skillPair) {
                tmp.push(skl);
            }
        }
        this.selectedSkills = tmp;
    }

    addLanguage(): void {
        if (this.selectedLanguages.every(value => value.language !== this.language.value)) {
            this.selectedLanguages.push(new LanguagePair(this.language.value, this.languageLevel.value));
        }
    }

    removeLanguage(languagePair: LanguagePair): void {
        const tmp: LanguagePair[] = [];
        for (const lng of this.selectedLanguages) {
            if (lng !== languagePair) {
                tmp.push(lng);
            }
        }
        this.selectedLanguages = tmp;
    }

    find(): void {
        this.ready = false;
        const criteria = new ProfileSearchCriteria([], [], [], []);
        this.selectedPositions.forEach(value => criteria.positions.push(new PositionSearchCriteria(value.id)));
        this.selectedSpecialties.forEach(value => criteria.specialties.push(new SpecialtySearchCriteria(value.id)));
        this.selectedSkills.forEach(value => criteria.skills.push(new SkillSearchCriteria(value.skill.id, value.skillLevel)));
        this.selectedLanguages.forEach(value => criteria.languages.push(new LanguageSearchCriteria(value.language.id, value.languageLevel)));
        this.profileService.searchProfiles(criteria).subscribe(
            profiles => {
                this.profiles = profiles;
                this.ready = true;
            },
            err => alert(this.security.getErrorMessage(err)));
    }
}

export class SkillPair {
    constructor(public skill: Skill, public skillLevel: string) {
    }
}

export class LanguagePair {
    constructor(public language: Language, public languageLevel: string) {
    }
}
