import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../profile-modify/profile-modify.component';
import {Position} from '../../../model/position/position';
import {Contact} from '../../../model/contact/contact';
import {Skill} from '../../../model/skill/skill';
import {Language} from '../../../model/language/language';
import {Experience} from '../../../model/experience/experience';
import {Education} from '../../../model/education/education';
import {Additional} from '../../../model/additional/additional';

@Component({
    selector: 'app-profile-show',
    templateUrl: './profile-show.component.html',
    styleUrls: ['./profile-show.component.css']
})
export class ProfileShowComponent implements OnInit {

    constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    ngOnInit(): void {
    }

    print(): void {
        const print = window.open();
        print?.document.write(document.getElementById('resume')?.innerHTML || '');
        print?.document.close();
        print?.print();
    }

    getContacts(type: string): Contact[] {
        const addresses: Contact[] = [];
        this.data.contacts.forEach(contact => {
            if (contact.type === type) {
                addresses.push(contact);
            }
        });
        return addresses;
    }

}

export class DialogData {

    constructor(
        public id: string,
        public name: string,
        public about: string,
        public position: Position,
        public contacts: Contact[],
        public skills: Map<Skill, string>,
        public languages: Map<Language, string>,
        public experience: Experience[],
        public education: Education[],
        public additional: Additional[]
    ) {
    }

}
