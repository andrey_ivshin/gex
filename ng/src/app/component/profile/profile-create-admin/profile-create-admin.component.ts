import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../service/user/user.service';
import {User} from '../../../model/user/user';
import {Router} from '@angular/router';
import {SecurityService} from '../../../service/security/security.service';
import {MatDialog} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'app-profile-create-admin',
    templateUrl: './profile-create-admin.component.html',
    styleUrls: ['./profile-create-admin.component.css']
})
export class ProfileCreateAdminComponent implements OnInit {

    constructor(private userService: UserService, router: Router,
                private security: SecurityService, private dialog: MatDialog) {
    }

    ready = false;
    users: User[] = [];
    user = new FormControl('', [
        Validators.required
    ]);
    form = new FormGroup({
        user: this.user
    });

    ngOnInit(): void {
        this.userService.getUsers().subscribe(
            users => {
                this.users = users;
                this.ready = true;
            },
            err => alert(this.security.getErrorMessage(err)));
    }

}
