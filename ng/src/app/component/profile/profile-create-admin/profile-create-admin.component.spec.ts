import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ProfileCreateAdminComponent} from './profile-create-admin.component';

describe('ProfileCreateAdminComponent', () => {
  let component: ProfileCreateAdminComponent;
  let fixture: ComponentFixture<ProfileCreateAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileCreateAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCreateAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
