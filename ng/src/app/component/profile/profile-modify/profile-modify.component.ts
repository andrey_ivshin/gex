import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ProfileService} from '../../../service/profile/profile.service';
import {ActivatedRoute} from '@angular/router';
import {Profile} from '../../../model/profile/profile';
import {ProfileTransfer} from '../../../model/profile-transfer/profile-transfer';
import {SecurityService} from '../../../service/security/security.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Position} from '../../../model/position/position';
import {PositionService} from '../../../service/position/position.service';
import {AreYouSureComponent} from '../../.stereotype/are-you-sure/are-you-sure.component';
import {MatDialog} from '@angular/material/dialog';
import {CompanyService} from '../../../service/company/company.service';
import {SpecialtyService} from '../../../service/specialty/specialty.service';
import {InstitutionService} from '../../../service/institution/institution.service';
import {SkillService} from '../../../service/skill/skill.service';
import {LanguageService} from '../../../service/language/language.service';
import {Company} from '../../../model/company/company';
import {Specialty} from '../../../model/specialty/specialty';
import {Institution} from '../../../model/institution/institution';
import {Skill} from '../../../model/skill/skill';
import {Language} from '../../../model/language/language';
import {Contact} from '../../../model/contact/contact';
import {ContactCreateComponent} from '../../contact/contact-create/contact-create.component';
import {ContactModifyComponent} from '../../contact/contact-modify/contact-modify.component';
import {ProfileSkillAddComponent} from '../../profile-skill/profile-skill-add/profile-skill-add.component';
import {ProfileSkillModifyComponent} from '../../profile-skill/profile-skill-modify/profile-skill-modify.component';
import {ProfileLanguageAddComponent} from '../../profile-language/profile-language-add/profile-language-add.component';
import {ProfileLanguageModifyComponent} from '../../profile-language/profile-language-modify/profile-language-modify.component';
import {Experience} from '../../../model/experience/experience';
import {Education} from '../../../model/education/education';
import {Additional} from '../../../model/additional/additional';
import {ExperienceCreateComponent} from '../../experience/experience-create/experience-create.component';
import {ExperienceModifyComponent} from '../../experience/experience-modify/experience-modify.component';
import {ProfileExperiencePositionSetComponent} from '../../profile-experience/profile-experience-position-set/profile-experience-position-set.component';
import {ProfileExperienceCompanySetComponent} from '../../profile-experience/profile-experience-company-set/profile-experience-company-set.component';
import {PointAddComponent} from '../../point/point-add/point-add.component';
import {PointModifyComponent} from '../../point/point-modify/point-modify.component';
import {Point} from '../../../model/point/point';
import {EducationCreateComponent} from '../../education/education-create/education-create.component';
import {EducationModifyComponent} from '../../education/education-modify/education-modify.component';
import {ProfileEducationInstitutionSetComponent} from '../../profile-education/profile-education-institution-set/profile-education-institution-set.component';
import {ProfileEducationSpecialtySetComponent} from '../../profile-education/profile-education-specialty-set/profile-education-specialty-set.component';
import {AdditionalCreateComponent} from '../../additional/additional-create/additional-create.component';
import {AdditionalModifyComponent} from '../../additional/additional-modify/additional-modify.component';
import {ProfileShowComponent} from '../profile-show/profile-show.component';

@Component({
    selector: 'app-profile-modify',
    templateUrl: './profile-modify.component.html',
    styleUrls: ['./profile-modify.component.css']
})
export class ProfileModifyComponent implements OnInit {

    constructor(private profileService: ProfileService, private activatedRoute: ActivatedRoute,
                private security: SecurityService, private snackBar: MatSnackBar,
                private positionService: PositionService, private dialog: MatDialog,
                private companyService: CompanyService, private specialtyService: SpecialtyService,
                private institutionService: InstitutionService, private skillService: SkillService,
                private languageService: LanguageService) {
    }

    id = 'none';
    ready = false;
    noProfile = false;

    profileSkills: Map<Skill, string> = new Map<Skill, string>();
    profileLanguages: Map<Language, string> = new Map<Language, string>();

    nonePosition = new Position('none', 'NONE');
    positions: Position[] = [];

    noneCompany = new Company('none', 'NONE');
    companies: Company[] = [];

    noneSpecialty = new Specialty('none', 'NONE');
    specialties: Specialty[] = [];

    noneInstitution = new Institution('none', 'NONE');
    institutions: Institution[] = [];

    contacts: Contact[] = [];
    skills: Skill[] = [];
    languages: Language[] = [];
    experience: Experience[] = [];
    education: Education[] = [];
    additional: Additional[] = [];

    name = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    about = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    summaryForm = new FormGroup({
        profileName: this.name,
        profileAbout: this.about
    });
    summaryServerError = false;
    summaryServerErrorMessage = '';

    position = new FormControl(this.nonePosition, [
        Validators.required,
    ]);
    positionForm = new FormGroup({
        position: this.position
    });
    positionServerError = false;
    positionServerErrorMessage = '';

    async ngOnInit(): Promise<void> {
        let skillsReady = false;
        let languagesReady = false;
        let positionsReady = false;
        let companiesReady = false;
        let specialtiesReady = false;
        let institutionsReady = false;
        let profileReady = false;
        let prof: any = null;

        this.skillService.getSkills().subscribe(
            skills => {
                this.skills = skills;
                skillsReady = true;
            }, err => alert(this.security.getErrorMessage(err)));

        this.languageService.getLanguages().subscribe(
            languages => {
                this.languages = languages;
                languagesReady = true;
            }, err => alert(this.security.getErrorMessage(err)));

        this.positionService.getPositions().subscribe(
            positions => {
                this.positions = positions;
                this.positions.push(this.nonePosition);
                positionsReady = true;
            }, err => alert(this.security.getErrorMessage(err)));

        this.companyService.getCompanies().subscribe(
            companies => {
                this.companies = companies;
                this.companies.push(this.noneCompany);
                companiesReady = true;
            }, err => alert(this.security.getErrorMessage(err)));

        this.specialtyService.getSpecialties().subscribe(
            specialties => {
                this.specialties = specialties;
                specialties.push(this.noneSpecialty);
                specialtiesReady = true;
            }, err => alert(this.security.getErrorMessage(err)));

        this.institutionService.getInstitutions().subscribe(
            institutions => {
                this.institutions = institutions;
                this.institutions.push(this.noneInstitution);
                institutionsReady = true;
            }, err => alert(this.security.getErrorMessage(err)));

        this.activatedRoute.paramMap.subscribe(value => this.id = value.get('profile-id') || 'none');
        this.profileService.getProfile(this.id).subscribe(
            profile => {
                prof = profile;
                profileReady = true;
            }, err => {
                if (this.security.getErrorCode(err) === 404) {
                    this.noProfile = true;
                } else {
                    alert(this.security.getErrorMessage(err));
                }
            });

        while (true) {
            if (skillsReady && languagesReady && positionsReady &&
                companiesReady && specialtiesReady && institutionsReady &&
                profileReady) {
                this.updateProfile(prof as Profile);
                this.ready = true;
                break;
            } else {
                await new Promise(resolve => setTimeout(resolve, 10));
            }
        }
    }

    updateProfile(profile: Profile): void {
        this.updateSummary(profile);
        this.updatePosition(profile);
        this.updateContacts(profile);
        this.updateProfileSkills(profile);
        this.updateProfileLanguages(profile);
        this.updateExperience(profile);
        this.updateEducation(profile);
        this.updateAdditional(profile);
    }

    updateSummary(profile: Profile): void {
        this.name.setValue(profile.name);
        this.about.setValue(profile.about);
    }

    updatePosition(profile: Profile): void {
        this.position.setValue(
            profile.position === null
                ? this.nonePosition
                : this.positions.find(position => position.id === profile.position.id));
    }

    updateContacts(profile: Profile): void {
        if (profile !== undefined
        ) {
            this.contacts = profile.contacts;
        }
    }

    updateProfileSkills(profile: Profile): void {
        if (profile !== undefined
        ) {
            this.profileSkills = new Map<Skill, string>();
            const json = JSON.stringify(profile.skills);
            if (json !== '{}') {
                const strings = json.replace('{', '')
                    .replace('}', '')
                    .split('","');
                for (const str of strings) {
                    const split = str.split('"').join('').split(':');
                    const skill = split[0].split(',');
                    this.profileSkills.set(new Skill(skill[0], skill[1]), split[1]);
                }
            }
        }
    }

    updateProfileLanguages(profile: Profile): void {
        if (profile !== undefined
        ) {
            this.profileLanguages = new Map<Language, string>();
            const json = JSON.stringify(profile.languages);
            if (json !== '{}') {
                const strings = json.replace('{', '')
                    .replace('}', '')
                    .split('","');
                for (const str of strings) {
                    const split = str.split('"').join('').split(':');
                    const language = split[0].split(',');
                    this.profileLanguages.set(new Language(language[0], language[1]), split[1]);
                }
            }
        }
    }

    updateExperience(profile: Profile): void {
        if (profile !== undefined
        ) {
            this.experience = profile.experience;
        }
    }

    updateEducation(profile: Profile): void {
        if (profile !== undefined
        ) {
            this.education = profile.education;
        }
    }

    updateAdditional(profile: Profile): void {
        if (profile !== undefined
        ) {
            this.additional = profile.additional;
        }
    }

    createProfile(): void {
        this.profileService.createProfile(new ProfileTransfer(
            this.id, 'Name', 'About'
        )).subscribe(
            () => window.location.reload(),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    removeProfile(): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.profileService.removeProfile(this.id).subscribe(
                    () => window.location.reload(),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

    modifySummary(): void {
        this.profileService.modifyProfile(new ProfileTransfer(
            this.id,
            this.name.value,
            this.about.value,
        )).subscribe(
            profile => {
                this.updateSummary(profile);
                this.notify('Summary saved');
            }, err => {
                this.summaryServerError = true;
                this.summaryServerErrorMessage = this.security.getErrorMessage(err);
            });
    }

    modifyPosition(): void {
        if (this.position.value === this.nonePosition
        ) {
            this.profileService.unsetPosition(this.id).subscribe(
                profile => {
                    this.updatePosition(profile);
                    this.notify('Position saved');
                },
                err => {
                    this.positionServerError = true;
                    this.positionServerErrorMessage = this.security.getErrorMessage(err);
                });
        } else {
            this.profileService.setPosition(this.id, (this.position.value as Position).id).subscribe(
                profile => {
                    this.updatePosition(profile);
                    this.notify('Position saved');
                }, err => {
                    this.positionServerError = true;
                    this.positionServerErrorMessage = this.security.getErrorMessage(err);
                });
        }
    }

    createContact(): void {
        const ref = this.dialog.open(ContactCreateComponent, {data: {profileId: this.id}});
        ref.afterClosed().subscribe(
            profile => {
                this.updateContacts(profile);
            },
            err => alert(this.security.getErrorMessage(err)));
    }

    modifyContact(contact: Contact): void {
        const ref = this.dialog.open(ContactModifyComponent, {data: {profileId: this.id, contact}});
        ref.afterClosed().subscribe(
            profile => this.updateContacts(profile),
            err => alert(this.security.getErrorMessage(err)));
    }

    removeContact(contactId: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.profileService.removeContact(this.id, contactId).subscribe(
                    profile => this.updateContacts(profile),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

    addProfileSkill(): void {
        const ref = this.dialog.open(ProfileSkillAddComponent, {data: {profileId: this.id, skills: this.skills}});
        ref.afterClosed().subscribe(
            profile => this.updateProfileSkills(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    modifyProfileSkill(skill: Skill, level: string): void {
        const ref = this.dialog.open(ProfileSkillModifyComponent, {data: {profileId: this.id, skill, level}});
        ref.afterClosed().subscribe(
            profile => this.updateProfileSkills(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    removeProfileSkill(skillId: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.profileService.unsetProfileSkill(this.id, skillId).subscribe(
                    profile => this.updateProfileSkills(profile),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }


    addProfileLanguage(): void {
        const ref = this.dialog.open(ProfileLanguageAddComponent, {
            data: {
                profileId: this.id,
                languages: this.languages
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateProfileLanguages(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    modifyProfileLanguage(languageId: string, level: string): void {
        const ref = this.dialog.open(ProfileLanguageModifyComponent, {
            data: {
                profileId: this.id,
                languageId,
                level
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateProfileLanguages(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    removeProfileLanguage(languageId: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.profileService.unsetProfileLanguage(this.id, languageId).subscribe(
                    profile => this.updateProfileLanguages(profile),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

    addExperience(): void {
        const ref = this.dialog.open(ExperienceCreateComponent, {data: {profileId: this.id}});
        ref.afterClosed().subscribe(
            profile => this.updateExperience(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    modifyExperience(experienceId: string, start: Date, stop: Date): void {
        const ref = this.dialog.open(ExperienceModifyComponent, {
            data: {
                profileId: this.id,
                experienceId,
                start,
                stop
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateExperience(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    removeExperience(experienceId: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.profileService.removeExperience(this.id, experienceId).subscribe(
                    profile => this.updateExperience(profile),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

    setExperiencePosition(experienceId: string, positionId: string): void {
        const ref = this.dialog.open(ProfileExperiencePositionSetComponent, {
            data: {
                profileId: this.id,
                experienceId,
                positionId,
                positions: this.positions
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateExperience(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    setExperienceCompany(experienceId: string, companyId: string): void {
        const ref = this.dialog.open(ProfileExperienceCompanySetComponent, {
            data: {
                profileId: this.id,
                experienceId,
                companyId,
                companies: this.companies
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateExperience(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    addExperiencePoint(experienceId: string): void {
        const ref = this.dialog.open(PointAddComponent, {
            data: {
                profileId: this.id,
                mode: 'exp',
                id: experienceId
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateExperience(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    modifyExperiencePoint(experienceId: string, point: Point): void {
        const ref = this.dialog.open(PointModifyComponent, {
            data: {
                profileId: this.id,
                mode: 'exp',
                id: experienceId,
                point
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateExperience(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    removeExperiencePoint(experienceId: string, pointId: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.profileService.removeExperiencePoint(this.id, experienceId, pointId).subscribe(
                    profile => this.updateExperience(profile),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }


    addEducation(): void {
        const ref = this.dialog.open(EducationCreateComponent, {data: {profileId: this.id}});
        ref.afterClosed().subscribe(
            profile => this.updateEducation(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    modifyEducation(educationId: string, start: Date, stop: Date): void {
        const ref = this.dialog.open(EducationModifyComponent, {
            data: {
                profileId: this.id,
                educationId,
                start,
                stop
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateEducation(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    removeEducation(educationId: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.profileService.removeEducation(this.id, educationId).subscribe(
                    profile => this.updateEducation(profile),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

    setEducationSpecialty(educationId: string, specialtyId: string): void {
        const ref = this.dialog.open(ProfileEducationSpecialtySetComponent, {
            data: {
                profileId: this.id,
                educationId,
                specialtyId,
                specialties: this.specialties
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateEducation(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    setEducationInstitution(educationId: string, institutionId: string): void {
        const ref = this.dialog.open(ProfileEducationInstitutionSetComponent, {
            data: {
                profileId: this.id,
                educationId,
                institutionId,
                institutions: this.institutions
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateEducation(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    addEducationPoint(educationId: string): void {
        const ref = this.dialog.open(PointAddComponent, {
            data: {
                profileId: this.id,
                mode: 'edu',
                id: educationId
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateEducation(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    modifyEducationPoint(educationId: string, point: Point): void {
        const ref = this.dialog.open(PointModifyComponent, {
            data: {
                profileId: this.id,
                mode: 'edu',
                id: educationId,
                point
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateEducation(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    removeEducationPoint(educationId: string, pointId: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.profileService.removeEducationPoint(this.id, educationId, pointId).subscribe(
                    profile => this.updateEducation(profile),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

    addAdditional(): void {
        const ref = this.dialog.open(AdditionalCreateComponent, {data: {profileId: this.id}});
        ref.afterClosed().subscribe(
            profile => this.updateAdditional(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    modifyAdditional(additionalId: string, name: string): void {
        const ref = this.dialog.open(AdditionalModifyComponent, {
            data: {
                profileId: this.id,
                name
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateAdditional(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    removeAdditional(additionalId: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.profileService.removeAdditional(this.id, additionalId).subscribe(
                    profile => this.updateAdditional(profile),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

    addAdditionalPoint(additionalId: string): void {
        const ref = this.dialog.open(PointAddComponent, {
            data: {
                profileId: this.id,
                mode: 'add',
                id: additionalId
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateAdditional(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    modifyAdditionalPoint(additionalId: string, point: Point): void {
        const ref = this.dialog.open(PointModifyComponent, {
            data: {
                profileId: this.id,
                mode: 'add',
                id: additionalId,
                point
            }
        });
        ref.afterClosed().subscribe(
            profile => this.updateAdditional(profile),
            err => alert(this.security.getErrorMessage(err))
        );
    }

    removeAdditionalPoint(additionalId: string, pointId: string): void {
        const ref = this.dialog.open(AreYouSureComponent);
        ref.afterClosed().subscribe(yes => {
            if (yes) {
                this.profileService.removeAdditionalPoint(this.id, additionalId, pointId).subscribe(
                    profile => this.updateAdditional(profile),
                    err => alert(this.security.getErrorMessage(err)));
            }
        });
    }

    notify(str: string): void {
        this.snackBar.open(str, 'Close', {duration: 1000});
    }

    show(): void {
        this.dialog.open(ProfileShowComponent, {
            data: {
                id: this.id,
                name: this.name.value,
                about: this.about.value,
                position: this.position.value,
                contacts: this.contacts,
                skills: this.profileSkills,
                languages: this.profileLanguages,
                experience: this.experience,
                education: this.education,
                additional: this.additional
            }
        });
    }

}
