import {Component, Inject, OnInit} from '@angular/core';
import {SecurityService} from '../../../service/security/security.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ProfileService} from '../../../service/profile/profile.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {PointCreationTransfer} from '../../../model/point-creation-transfer/point-creation-transfer';

@Component({
    selector: 'app-point-add',
    templateUrl: './point-add.component.html',
    styleUrls: ['./point-add.component.css']
})
export class PointAddComponent implements OnInit {

    constructor(private profileService: ProfileService, private security: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    content = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.content
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
    }

    create(): void {
        if (this.data.mode === 'exp') {
            this.profileService.addExperiencePoint(this.data.profileId, this.data.id,
                new PointCreationTransfer(this.content.value)).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        }
        if (this.data.mode === 'edu') {
            this.profileService.addEducationPoint(this.data.profileId, this.data.id,
                new PointCreationTransfer(this.content.value)).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        }
        if (this.data.mode === 'add') {
            this.profileService.addAdditionalPoint(this.data.profileId, this.data.id,
                new PointCreationTransfer(this.content.value)).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        }
    }

}

export class DialogData {

    constructor(public profileId: string, public mode: string, public id: string) {
    }

}
