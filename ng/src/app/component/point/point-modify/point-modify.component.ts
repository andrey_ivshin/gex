import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from '../../../service/profile/profile.service';
import {SecurityService} from '../../../service/security/security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Point} from '../../../model/point/point';
import {PointModificationTransfer} from '../../../model/point-modification-transfer/point-modification-transfer';

@Component({
    selector: 'app-point-modify',
    templateUrl: './point-modify.component.html',
    styleUrls: ['./point-modify.component.css']
})
export class PointModifyComponent implements OnInit {

    constructor(private profileService: ProfileService, private security: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    content = new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(1000)
    ]);
    form = new FormGroup({
        name: this.content
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.content.setValue(this.data.point.content);
    }

    modify(): void {
        if (this.data.mode === 'exp') {
            this.profileService.modifyExperiencePoint(this.data.profileId, this.data.id,
                new PointModificationTransfer(this.data.point.id, this.content.value)).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        }
        if (this.data.mode === 'edu') {
            this.profileService.modifyEducationPoint(this.data.profileId, this.data.id,
                new PointModificationTransfer(this.data.point.id, this.content.value)).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        }
        if (this.data.mode === 'add') {
            this.profileService.modifyAdditionalPoint(this.data.profileId, this.data.id,
                new PointModificationTransfer(this.data.point.id, this.content.value)).subscribe(
                profile => this.ref.close(profile),
                err => {
                    this.serverError = true;
                    this.serverErrorMessage = this.security.getErrorMessage(err);
                });
        }
    }

}

export class DialogData {

    constructor(public profileId: string, public mode: string, public id: string, public point: Point) {
    }

}
