import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PointModifyComponent } from './point-modify.component';

describe('PointModifyComponent', () => {
  let component: PointModifyComponent;
  let fixture: ComponentFixture<PointModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PointModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PointModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
