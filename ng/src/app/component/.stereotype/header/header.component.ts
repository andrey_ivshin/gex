import {Component, OnInit} from '@angular/core';
import {SecurityService} from '../../../service/security/security.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    constructor(public security: SecurityService, private router: Router) {
    }

    ngOnInit(): void {
    }

    logOut(): void {
        this.security.logout().subscribe(
            res => {
                this.router.navigate(['/']);
            }, err => {
                alert(this.security.getErrorMessage(err));
            });
    }

}
