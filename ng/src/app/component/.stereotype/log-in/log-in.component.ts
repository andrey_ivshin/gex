import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SecurityService} from '../../../service/security/security.service';
import {UserAuthorizationRequest} from '../../../model/user-authorization-request/user-authorization-request';
import {Router} from '@angular/router';

@Component({
    selector: 'app-log-in',
    templateUrl: './log-in.component.html',
    styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

    constructor(private security: SecurityService, private router: Router) {
    }

    serverError = false;
    serverErrorMessage = '';
    email = new FormControl('', [
        Validators.required,
        Validators.email
    ]);
    password = new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50)
    ]);
    form = new FormGroup({
        email: this.email,
        password: this.password
    });

    ngOnInit(): void {
    }

    logIn(): void {
        this.security.authorize(new UserAuthorizationRequest(this.email.value, this.password.value))
            .subscribe(res => {
                this.router.navigate(['/']);
            }, err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}
