import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileLanguageAddComponent } from './profile-language-add.component';

describe('ProfileLanguageAddComponent', () => {
  let component: ProfileLanguageAddComponent;
  let fixture: ComponentFixture<ProfileLanguageAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileLanguageAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileLanguageAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
