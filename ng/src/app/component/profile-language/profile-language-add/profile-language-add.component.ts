import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from '../../../service/profile/profile.service';
import {LanguageService} from '../../../service/language/language.service';
import {SecurityService} from '../../../service/security/security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {Language} from '../../../model/language/language';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LanguageLevelTransfer} from '../../../model/language-level-transfer/language-level-transfer';
import {LanguageLevel} from '../../../model/language-level/language-level';

@Component({
  selector: 'app-profile-language-add',
  templateUrl: './profile-language-add.component.html',
  styleUrls: ['./profile-language-add.component.css']
})
export class ProfileLanguageAddComponent implements OnInit {

    constructor(private profileService: ProfileService, private languageService: LanguageService, private security: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    languages: Language[] = [];
    levels = LanguageLevel.values;
    language = new FormControl('', [
        Validators.required
    ]);
    level = new FormControl('', [
        Validators.required
    ]);
    form = new FormGroup({
        language: this.language,
        level: this.level
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.languages = this.data.languages;
    }

    add(): void {
        this.profileService.setProfileLanguage(this.data.profileId, (this.language.value as Language).id,
            new LanguageLevelTransfer(this.level.value)).subscribe(
            profile => this.ref.close(profile),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}

export class DialogData {

    constructor(public profileId: string, public languages: Language[]) {
    }

}
