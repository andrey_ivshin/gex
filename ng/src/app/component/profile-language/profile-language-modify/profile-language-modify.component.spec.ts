import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileLanguageModifyComponent } from './profile-language-modify.component';

describe('ProfileLanguageModifyComponent', () => {
  let component: ProfileLanguageModifyComponent;
  let fixture: ComponentFixture<ProfileLanguageModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileLanguageModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileLanguageModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
