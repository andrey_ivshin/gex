import {Component, Inject, OnInit} from '@angular/core';
import {ProfileService} from '../../../service/profile/profile.service';
import {LanguageService} from '../../../service/language/language.service';
import {SecurityService} from '../../../service/security/security.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ProfileModifyComponent} from '../../profile/profile-modify/profile-modify.component';
import {LanguageLevel} from '../../../model/language-level/language-level';
import {Language} from '../../../model/language/language';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LanguageLevelTransfer} from '../../../model/language-level-transfer/language-level-transfer';

@Component({
    selector: 'app-profile-language-modify',
    templateUrl: './profile-language-modify.component.html',
    styleUrls: ['./profile-language-modify.component.css']
})
export class ProfileLanguageModifyComponent implements OnInit {


    constructor(private profileService: ProfileService, private languageService: LanguageService, private security: SecurityService,
                @Inject(MAT_DIALOG_DATA) public data: DialogData, private ref: MatDialogRef<ProfileModifyComponent>) {
    }

    levels = LanguageLevel.values;
    language: Language = new Language('', '');
    level = new FormControl('', [
        Validators.required
    ]);
    form = new FormGroup({
        level: this.level
    });
    serverError = false;
    serverErrorMessage = '';

    ngOnInit(): void {
        this.language = this.data.language;
        this.levels.forEach(level => {
            if (this.data.level === level) {
                this.level.setValue(level);
            }
        });
    }

    modify(): void {
        this.profileService.setProfileLanguage(this.data.profileId, this.data.language.id,
            new LanguageLevelTransfer(this.level.value)).subscribe(
            profile => this.ref.close(profile),
            err => {
                this.serverError = true;
                this.serverErrorMessage = this.security.getErrorMessage(err);
            });
    }

}

export class DialogData {

    constructor(public profileId: string, public language: Language, public level: string) {
    }

}
