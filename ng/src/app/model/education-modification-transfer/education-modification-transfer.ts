export class EducationModificationTransfer {

    constructor(public id: string, public start: Date, public stop: Date | null) {
    }

}
