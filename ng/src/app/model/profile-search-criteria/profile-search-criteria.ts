export class ProfileSearchCriteria {

    constructor(public positions: PositionSearchCriteria[], public specialties: SpecialtySearchCriteria[],
                public skills: SkillSearchCriteria[], public languages: LanguageSearchCriteria[]) {
    }

}

export class PositionSearchCriteria {

    constructor(public positionId: string){
    }

}

export class SpecialtySearchCriteria {

    constructor(public specialtyId: string){
    }

}

export class SkillSearchCriteria {

    constructor(public skillId: string, public skillLevel: string) {
    }

}

export class LanguageSearchCriteria {

    constructor(public languageId: string, public languageLevel: string) {
    }

}

