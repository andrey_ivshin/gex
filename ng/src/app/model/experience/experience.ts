import {Point} from '../point/point';
import {Company} from '../company/company';
import {Position} from '../position/position';

export class Experience {

    constructor(public id: string, public company: Company, public position: Position,
                public start: Date, public stop: Date, public points: Point[]) {
    }

}
