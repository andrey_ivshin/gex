export class LanguageLevel {

    public static values: string[] = ['A1', 'A2', 'B1', 'B2', 'C1', 'C2', 'NATIVE'];

}
