export class ContactType {

    public static values: string[] = ['ADDRESS', 'EMAIL', 'PHONE'];

}
