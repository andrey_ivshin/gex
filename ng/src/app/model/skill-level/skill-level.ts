export class SkillLevel {

    public static values: string[] = ['BASIC', 'NOVICE', 'INTERMEDIATE', 'ADVANCED', 'EXPERT'];

}
