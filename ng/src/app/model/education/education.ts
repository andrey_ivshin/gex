import {Institution} from '../institution/institution';
import {Specialty} from '../specialty/specialty';
import {Point} from '../point/point';

export class Education {

    constructor(public id: string, public institution: Institution, public specialty: Specialty,
                public start: Date, public stop: Date, public points: Point[]) {
    }

}
