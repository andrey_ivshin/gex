import {Profile} from '../profile/profile';
import {Language} from '../language/language';

export class ProfileLanguage {

    constructor(public profile: Profile, public language: Language, public level: string) {
    }

}
