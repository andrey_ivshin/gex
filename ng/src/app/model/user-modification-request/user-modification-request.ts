export class UserModificationRequest {

    constructor(public id: string, public email: string, public password: string, public passwordConfirmation: string, public role: string) {
    }

}
