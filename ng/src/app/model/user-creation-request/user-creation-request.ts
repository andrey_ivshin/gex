export class UserCreationRequest {

    constructor(private email: string, private password: string, private passwordConfirmation: string, private role: string) {
    }

}
