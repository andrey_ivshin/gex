export class ExperienceModificationTransfer {

    constructor(public id: string, public start: Date, public stop: Date | null) {
    }

}
