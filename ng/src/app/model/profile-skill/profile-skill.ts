import {Skill} from '../skill/skill';
import {Profile} from '../profile/profile';

export class ProfileSkill {

    constructor(public profile: Profile, public skill: Skill, public level: string) {
    }

}
