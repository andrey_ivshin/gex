import {Point} from '../point/point';

export class Additional {

    constructor(public id: string, public name: string, public points: Point[]) {
    }

}
