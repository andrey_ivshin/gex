import {Position} from '../position/position';
import {Contact} from '../contact/contact';
import {Experience} from '../experience/experience';
import {Education} from '../education/education';
import {Additional} from '../additional/additional';

export class Profile {

    constructor(public id: string, public name: string, public about: string,
                public position: Position, public contacts: Contact[],
                public skills: Map<string, string>, public languages: Map<string, string>,
                public experience: Experience[], public education: Education[], public additional: Additional[]) {
    }

}
