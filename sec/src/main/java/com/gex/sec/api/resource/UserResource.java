package com.gex.sec.api.resource;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class UserResource {

    String id;
    String email;
    String role;

    @FieldDefaults(level = AccessLevel.PUBLIC, makeFinal = true)
    @AllArgsConstructor
    public static class AuthorizationRequest {

        String email;
        String password;

    }

    @FieldDefaults(level = AccessLevel.PUBLIC, makeFinal = true)
    @AllArgsConstructor
    public static class RegistrationRequest {

        String email;
        String password;
        String passwordConfirmation;
        String role;

    }

    @FieldDefaults(level = AccessLevel.PUBLIC, makeFinal = true)
    @AllArgsConstructor
    public static class ModificationRequest {

        String id;
        String email;
        String password;
        String passwordConfirmation;
        String role;

    }

}
