package com.gex.sec.api.controller;

import com.gex.sec.api.mapper.UserResourceMapper;
import com.gex.sec.api.resource.UserResource;
import com.gex.sec.api.security.Role;
import com.gex.sec.api.security.SecurityUtil;
import com.gex.sec.domain.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

import static com.gex.sec.api.mapper.SimpleUserResourceMapper.UserResourceMappingException;
import static com.gex.sec.api.resource.UserResource.*;
import static com.gex.sec.domain.model.User.*;
import static com.gex.sec.domain.service.SimpleUserService.*;
import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class UserController {

    private final UserService service;
    private final SecurityUtil security;
    private final UserResourceMapper mapper;

    @GetMapping("init")
    public void init() {
        service.register(new Email("email@email.com"), new Password("password"), Role.ADMIN);
    }

    @GetMapping("user")
    public ResponseEntity<List<UserResource>> getAll() {
        return security.admin(token -> ResponseEntity.ok(service.getAll().stream()
                .map(mapper::map).collect(Collectors.toList())));
    }

    @GetMapping("user/{id}")
    public ResponseEntity<UserResource> get(@PathVariable String id) {
        try {
            return security.admin(token -> ResponseEntity.ok(mapper.map(service.get(new Id(id)))));
        } catch (ConstraintViolationException e) {
            return security.error(BAD_REQUEST, e.getMessage());
        } catch (UserNotFoundException e) {
            return security.error(NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("authorize")
    public ResponseEntity<Void> authorize(@RequestBody AuthorizationRequest request) {
        try {
            return security.authorize(mapper.mapToken(service.authorize(
                    new Email(request.email),
                    new Password(request.password))));
        } catch (ConstraintViolationException e) {
            return security.error(BAD_REQUEST, e.getMessage());
        } catch (UserNotFoundException e) {
            return security.error(NOT_FOUND, e.getMessage());
        } catch (WrongPasswordException e) {
            return security.error(FORBIDDEN, e.getMessage());
        }
    }

    @PostMapping("logout")
    public ResponseEntity<Void> logout() {
        return security.logout();
    }

    @PostMapping("user")
    public ResponseEntity<UserResource> register(@RequestBody RegistrationRequest request) {
        try {
            return security.admin(token -> {
                confirmPassword(request.password, request.passwordConfirmation);
                return ResponseEntity.ok(mapper.map(service.register(
                        new Email(request.email), new Password(request.password), mapper.mapRole(request.role))));
            });
        } catch (ConstraintViolationException | UserResourceMappingException |
                PasswordNotConfirmedException | EmailAlreadyUsedException e) {
            return security.error(BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("user")
    public ResponseEntity<UserResource> modify(@RequestBody ModificationRequest request) {
        try {
            return security.admin(token -> {
                confirmPassword(request.password, request.passwordConfirmation);
                return ResponseEntity.ok(mapper.map(service.modify(
                        new Id(request.id),
                        new Email(request.email),
                        new Password(request.password),
                        mapper.mapRole(request.role))));
            });
        } catch (ConstraintViolationException
                | UserResourceMappingException
                | PasswordNotConfirmedException
                | EmailAlreadyUsedException e) {
            return security.error(BAD_REQUEST, e.getMessage());
        } catch (UserNotFoundException e) {
            return security.error(NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("user/{id}")
    public ResponseEntity<Void> remove(@PathVariable String id) {
        try {
            return security.admin(token -> {
                service.remove(new Id(id));
                return ResponseEntity.ok().build();
            });
        } catch (ConstraintViolationException e) {
            return security.error(BAD_REQUEST, e.getMessage());
        } catch (UserNotFoundException e) {
            return security.error(NOT_FOUND, e.getMessage());
        }
    }

    private void confirmPassword(String password, String passwordConfirmation) {
        try {
            if (!password.equals(passwordConfirmation))
                throw new PasswordNotConfirmedException("password not confirmed");
        } catch (NullPointerException e) {
            throw new PasswordNotConfirmedException("password must not be null");
        }
    }

    public static class PasswordNotConfirmedException extends RuntimeException {
        public PasswordNotConfirmedException(String message) {
            super(message);
        }
    }

}
