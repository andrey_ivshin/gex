package com.gex.sec.api.mapper;

import com.gex.sec.api.resource.UserResource;
import com.gex.sec.api.security.Role;
import com.gex.sec.api.security.Token;
import com.gex.sec.domain.model.User;
import org.springframework.stereotype.Component;

@Component
public class SimpleUserResourceMapper implements UserResourceMapper {

    @Override
    public UserResource map(User user) {
        return new UserResource(
                user.getId().getValue(),
                user.getEmail().getValue(),
                user.getRole().name()
        );
    }

    @Override
    public Token mapToken(User user) {
        return new Token(user.getId().getValue(), user.getRole().name());
    }

    public Role mapRole(String string) {
        try {
            return Role.valueOf(string);
        } catch (IllegalArgumentException e) {
            throw new UserResourceMappingException("role not fond");
        } catch (NullPointerException e) {
            throw new UserResourceMappingException("role must not be null");
        }
    }

    public static class UserResourceMappingException extends RuntimeException {

        public UserResourceMappingException(String message) {
            super(message);
        }

    }

}
