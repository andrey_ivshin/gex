package com.gex.sec.api.mapper;

import com.gex.sec.api.resource.UserResource;
import com.gex.sec.api.security.Role;
import com.gex.sec.api.security.Token;
import com.gex.sec.domain.model.User;

public interface UserResourceMapper {

    UserResource map(User user);

    Token mapToken(User user);

    Role mapRole(String string);

}
