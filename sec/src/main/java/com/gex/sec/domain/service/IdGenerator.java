package com.gex.sec.domain.service;

import static com.gex.sec.domain.model.User.Id;

public interface IdGenerator {

    Id generate();

}
