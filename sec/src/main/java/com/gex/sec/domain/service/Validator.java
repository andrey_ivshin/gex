package com.gex.sec.domain.service;

import com.gex.sec.api.security.Role;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Validated
public interface Validator {

    default Validator validateId(@NotNull @Size(min = 36, max = 36) String id) {
        return this;
    }

    default Validator validateEmail(@NotNull @Email String email) {
        return this;
    }

    default Validator validatePassword(@NotNull @Size(min = 8, max = 50) String password) {
        return this;
    }

    default Validator validateRole(@NotNull Role role) {
        return this;
    }

}

