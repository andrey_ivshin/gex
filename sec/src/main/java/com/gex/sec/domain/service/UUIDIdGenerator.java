package com.gex.sec.domain.service;

import org.springframework.stereotype.Component;

import java.util.UUID;

import static com.gex.sec.domain.model.User.Id;


@Component
public class UUIDIdGenerator implements IdGenerator {

    @Override
    public Id generate() {
        return new Id(UUID.randomUUID().toString());
    }

}
