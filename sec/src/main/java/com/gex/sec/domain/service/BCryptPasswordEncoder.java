package com.gex.sec.domain.service;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Component;

import static com.gex.sec.domain.model.User.Password;

@Component
public class BCryptPasswordEncoder implements PasswordEncoder {

    @Override
    public Password encode(Password password) {
        return new Password(BCrypt.hashpw(password.getValue(), BCrypt.gensalt()));
    }

    @Override
    public boolean check(Password password, Password encodedPassword) {
        return BCrypt.checkpw(password.getValue(), encodedPassword.getValue());
    }

}
