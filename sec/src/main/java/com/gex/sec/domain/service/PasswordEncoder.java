package com.gex.sec.domain.service;

import static com.gex.sec.domain.model.User.Password;

public interface PasswordEncoder {

    Password encode(Password password);

    boolean check(Password password, Password encodedPassword);

}
