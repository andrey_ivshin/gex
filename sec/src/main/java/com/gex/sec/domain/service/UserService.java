package com.gex.sec.domain.service;

import com.gex.sec.api.security.Role;
import com.gex.sec.domain.model.User;

import java.util.List;

import static com.gex.sec.domain.model.User.*;

public interface UserService {

    List<User> getAll();

    User get(Id id);

    User authorize(Email email, Password password);

    User register(Email email, Password password, Role role);

    User modify(Id id, Email email, Password password,  Role role);

    void remove(Id id);

}
