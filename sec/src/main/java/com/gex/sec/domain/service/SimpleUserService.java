package com.gex.sec.domain.service;

import com.gex.sec.api.security.Role;
import com.gex.sec.domain.model.User;
import com.gex.sec.persistence.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

import static com.gex.sec.domain.model.User.*;

@Service
@RequiredArgsConstructor
public class SimpleUserService implements UserService {

    private final UserRepository repository;
    private final PasswordEncoder encoder;
    private final IdGenerator generator;
    private final Validator validator;

    @Override
    @Transactional
    public List<User> getAll() {
        return repository.findAll();
    }

    @Override
    @Transactional
    public User get(Id id) {
        validator.validateId(id.getValue());
        return repository.find(id)
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    @Transactional
    public User authorize(Email email, Password password) {
        validator.validateEmail(email.getValue()).validatePassword(password.getValue());
        return repository.find(email)
                .map(user -> {
                    if (encoder.check(password, user.getPassword()))
                        return user;
                    else
                        throw new WrongPasswordException();
                })
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    @Transactional
    public User register(Email email, Password password, Role role) {
        validator
                .validateEmail(email.getValue())
                .validatePassword(password.getValue())
                .validateRole(role);
        if (repository.find(email).isPresent())
            throw new EmailAlreadyUsedException();
        else
            return repository.create(new User(
                    generator.generate(),
                    email,
                    encoder.encode(password),
                    role
            ));
    }

    @Override
    @Transactional
    public User modify(Id id, Email email, Password password, Role role) {
        validator.validateId(id.getValue()).validateEmail(email.getValue()).validatePassword(password.getValue()).validateRole(role);
        return repository.find(id)
                .map(userById -> {
                    Optional<User> optionalUserByEmail = repository.find(email);
                    if (optionalUserByEmail.isPresent() &&
                            !optionalUserByEmail.get().getEmail().equals(userById.getEmail()))
                        throw new EmailAlreadyUsedException();
                    return repository.update(new User(
                            id,
                            email,
                            encoder.encode(password),
                            role
                    ));
                })
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    @Transactional
    public void remove(Id id) {
        validator.validateId(id.getValue());
        repository.find(id).orElseThrow(UserNotFoundException::new);
        repository.delete(id);
    }

    public static class UserNotFoundException extends RuntimeException {
        public UserNotFoundException() {
            super("user not found");
        }
    }

    public static class WrongPasswordException extends RuntimeException {
        public WrongPasswordException() {
            super("wrong password");
        }
    }

    public static class EmailAlreadyUsedException extends RuntimeException {
        public EmailAlreadyUsedException() {
            super("email already used");
        }
    }

}
