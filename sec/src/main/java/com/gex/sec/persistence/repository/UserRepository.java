package com.gex.sec.persistence.repository;

import com.gex.sec.domain.model.User;

import java.util.List;
import java.util.Optional;

import static com.gex.sec.domain.model.User.Email;
import static com.gex.sec.domain.model.User.Id;

public interface UserRepository {

    List<User> findAll();

    Optional<User> find(Id id);

    Optional<User> find(Email email);

    User create(User user);

    User update(User user);

    void delete(Id id);

}
