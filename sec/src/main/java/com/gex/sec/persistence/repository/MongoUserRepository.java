package com.gex.sec.persistence.repository;

import com.gex.sec.domain.model.User;
import com.gex.sec.persistence.mapper.UserEntityMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.gex.sec.domain.model.User.Email;
import static com.gex.sec.domain.model.User.Id;

@Repository
@RequiredArgsConstructor
public class MongoUserRepository implements UserRepository {

    private final MongoUserRepositoryDelegate delegate;
    private final UserEntityMapper mapper;
    
    @Override
    public List<User> findAll() {
        return delegate.findAll()
                .stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<User> find(Id id) {
        return delegate.findById(id.getValue())
                .map(mapper::map);
    }

    @Override
    public Optional<User> find(Email email) {
        return delegate.findByEmail(email.getValue())
                .map(mapper::map);
    }

    @Override
    public User create(User user) {
        return mapper.map(delegate.insert(mapper.map(user)));
    }

    @Override
    public User update(User user) {
        return mapper.map(delegate.save(mapper.map(user)));
    }

    @Override
    public void delete(Id id) {
        delegate.deleteById(id.getValue());
    }
    
}
