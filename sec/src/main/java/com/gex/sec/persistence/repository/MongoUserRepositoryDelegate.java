package com.gex.sec.persistence.repository;

import com.gex.sec.persistence.entity.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface MongoUserRepositoryDelegate extends MongoRepository<UserEntity, String> {

    Optional<UserEntity> findByEmail(String email);

}
