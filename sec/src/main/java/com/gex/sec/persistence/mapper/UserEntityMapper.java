package com.gex.sec.persistence.mapper;

import com.gex.sec.domain.model.User;
import com.gex.sec.persistence.entity.UserEntity;

public interface UserEntityMapper {

    User map(UserEntity entity);

    UserEntity map(User user);

}
