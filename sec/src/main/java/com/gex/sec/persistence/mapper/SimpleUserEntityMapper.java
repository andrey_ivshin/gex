package com.gex.sec.persistence.mapper;

import com.gex.sec.api.security.Role;
import com.gex.sec.domain.model.User;
import com.gex.sec.persistence.entity.UserEntity;
import org.springframework.stereotype.Component;

import static com.gex.sec.domain.model.User.*;

@Component
public class SimpleUserEntityMapper implements UserEntityMapper {

    @Override
    public User map(UserEntity entity) {
        return new User(
                new Id(entity.getId()),
                new Email(entity.getEmail()),
                new Password(entity.getPassword()),
                Role.valueOf(entity.getRole()));
    }

    @Override
    public UserEntity map(User user) {
        return new UserEntity(
                user.getId().getValue(),
                user.getEmail().getValue(),
                user.getPassword().getValue(),
                user.getRole().name());
    }

}
