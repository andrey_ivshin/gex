package com.gex.sec.persistence.repository;

import com.gex.sec.api.security.Role;
import com.gex.sec.domain.model.User;
import com.gex.sec.persistence.entity.UserEntity;
import com.gex.sec.persistence.mapper.UserEntityMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.gex.sec.TestUtils.USERS;
import static com.gex.sec.domain.model.User.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserRepositoryTest {

    @Autowired
    MongoUserRepositoryDelegate delegate;

    @Autowired
    UserEntityMapper mapper;

    @Autowired
    UserRepository repository;

    @Test
    @DisplayName("find all users")
    @Transactional
    void findAll() {
        List<User> users = repository.findAll();

        assertEquals(USERS, users);
    }

    @Test
    @DisplayName("return empty list if no user found")
    @Transactional
    void findAllEmpty() {
        delegate.deleteAll();

        List<User> users = repository.findAll();

        assertTrue(users.isEmpty());
    }

    @Test
    @DisplayName("find user by id")
    @Transactional
    void findById() {
        User target = USERS.get(1);

        Optional<User> user = repository.find(target.getId());

        assertTrue(user.isPresent());
        assertEquals(target, user.get());
    }

    @Test
    @DisplayName("return empty optional if no user with such id found")
    @Transactional
    void findByIdEmpty() {
        Optional<User> user = repository.find(new Id("4"));

        assertTrue(user.isEmpty());
    }

    @Test
    @DisplayName("find user by email")
    @Transactional
    void findByEmail() {
        User target = USERS.get(1);

        Optional<User> user = repository.find(target.getEmail());

        assertTrue(user.isPresent());
        assertEquals(target, user.get());
    }

    @Test
    @DisplayName("return empty optional if no user with such email found")
    @Transactional
    void findByEmailEmpty() {
        Optional<User> user = repository.find(new Id("4email@email.com"));

        assertTrue(user.isEmpty());
    }

    @Test
    @DisplayName("create user and return")
    @Transactional
    void create() {
        User creating = new User(new Id("4"), new Email("4email@email.com"), new Password("password"), Role.USER);

        User returned = repository.create(creating);

        Optional<UserEntity> persisted = delegate.findById(creating.getId().getValue());
        assertTrue(persisted.isPresent());
        assertEquals(creating, mapper.map(persisted.get()));
        assertEquals(creating, returned);
    }

    @Test
    @DisplayName("update user and return")
    @Transactional
    void update() {
        User updating = USERS.get(1);
        updating = new User(updating.getId(), updating.getEmail(), updating.getPassword(), updating.getRole());

        User returned = repository.update(updating);

        Optional<UserEntity> persisted = delegate.findById(updating.getId().getValue());
        assertTrue(persisted.isPresent());
        assertEquals(updating, mapper.map(persisted.get()));
        assertEquals(updating, returned);
    }

    @Test
    @DisplayName("delete user")
    @Transactional
    void delete() {
        User target = USERS.get(1);
        delegate.deleteById(target.getId().getValue());
        Optional<UserEntity> user = delegate.findById(target.getId().getValue());

        assertFalse(user.isPresent());
    }

}