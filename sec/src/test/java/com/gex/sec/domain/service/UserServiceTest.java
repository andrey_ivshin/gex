package com.gex.sec.domain.service;

import com.gex.sec.api.security.Role;
import com.gex.sec.domain.model.User;
import com.gex.sec.persistence.entity.UserEntity;
import com.gex.sec.persistence.repository.MongoUserRepositoryDelegate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Optional;

import static com.gex.sec.TestUtils.*;
import static com.gex.sec.domain.model.User.*;
import static com.gex.sec.domain.service.SimpleUserService.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserServiceTest {

    @Autowired
    UserService service;

    @Autowired
    MongoUserRepositoryDelegate delegate;

    @Test
    @DisplayName("get all users")
    @Transactional
    void getAll() {
        List<User> users = service.getAll();

        assertEquals(USERS, users);
    }

    @Test
    @DisplayName("return empty list if no users found")
    @Transactional
    void getAllEmpty() {
        delegate.deleteAll();

        List<User> users = service.getAll();

        assertTrue(users.isEmpty());
    }

    @Test
    @DisplayName("return user")
    @Transactional
    void get() {
        User target = USERS.get(1);

        User user = service.get(target.getId());

        assertEquals(target, user);
    }

    @Test
    @DisplayName("throw UserNotFoundException if user not found")
    @Transactional
    void getEmpty() {
        Id id = new Id("..................................36");
        assertThrows(UserNotFoundException.class, () -> service.get(id));
    }

    @Test
    @DisplayName("throw ConstraintViolationException if Id is invalid")
    @Transactional
    void getInvalid() {
        for (int i = 0; i < IDS.size(); i++) {
            if (i == 0)
                continue;
            Id id = IDS.get(i);
            assertThrows(ConstraintViolationException.class, () -> service.get(id));
        }
    }

    @Test
    @DisplayName("must be authorized")
    @Transactional
    void authorize() {
        User target = USERS.get(1);

        User authorized = service.authorize(target.getEmail(), new Password("password"));

        assertEquals(target, authorized);
    }

    @Test
    @DisplayName("if no user with provided email found UserNotFoundException must be thrown")
    @Transactional
    void authorizeEmpty() {
        Email email = new Email("4email@email.com");
        Password password = new Password("password");

        assertThrows(UserNotFoundException.class, () -> service.authorize(email, password));
    }

    @Test
    @DisplayName("if password is wrong WrongPasswordException must be thrown")
    @Transactional
    void authorizeWrong() {
        User target = USERS.get(1);
        Password password = new Password("password1");
        Email email = target.getEmail();

        assertThrows(WrongPasswordException.class, () -> service.authorize(email, password));
    }

    @Test
    @DisplayName("if email and password are invalid ConstraintViolationException must be thrown")
    @Transactional
    void authorizeInvalid() {
        for (int i = 0; i < EMAILS.size(); i++) {
            for (int j = 0; j < PASSWORDS.size(); j++) {
                if (i == 0 && j == 0)
                    continue;
                Email email = EMAILS.get(i);
                Password password = PASSWORDS.get(j);
                assertThrows(ConstraintViolationException.class, () -> service.authorize(email, password));
            }
        }
    }

    @Test
    @DisplayName("must be registered")
    @Transactional
    void register() {
        Email email = new Email("4email@email.com");
        Password password = new Password("password");

        User registered = service.register(email, password, Role.USER);

        assertEquals(email, registered.getEmail());
    }

    @Test
    @DisplayName("if email already used EmailAlreadyUsedException must thrown")
    @Transactional
    void registerUsed() {
        Email email = USERS.get(1).getEmail();
        Password password = new Password("password");

        assertThrows(EmailAlreadyUsedException.class, () -> service.register(email, password, Role.USER));
    }

    @Test
    @DisplayName("if email, password and role are invalid ConstraintViolationException must be thrown")
    @Transactional
    void registerInvalid() {
        for (int i = 0; i < EMAILS.size(); i++) {
            for (int j = 0; j < PASSWORDS.size(); j++) {
                for (int k = 0; k < ROLES.size(); k++) {
                    if (i == 0 && j == 0 && k == 0)
                        continue;
                    Email email = EMAILS.get(i);
                    Password password = PASSWORDS.get(j);
                    Role role = ROLES.get(k);
                    assertThrows(ConstraintViolationException.class, () -> service.register(email, password, role));
                }
            }
        }
    }

    @Test
    @DisplayName("must be modified")
    @Transactional
    void modify() {
        User target = USERS.get(1);
        Id id = target.getId();
        Email email = new Email("4email@email.com");
        Password password = new Password("password");
        Role role = target.getRole();

        User modified = service.modify(id, email, password, role);

        assertEquals(email, modified.getEmail());
    }

    @Test
    @DisplayName("if user not exist UserNotFoundException must thrown")
    @Transactional
    void modifyEmpty() {
        Id id = new Id("..................................36");
        Email email = new Email("4email@email.com");
        Password password = new Password("password");
        Role role = Role.USER;

        assertThrows(UserNotFoundException.class, () -> service.modify(id, email, password, role));
    }

    @Test
    @DisplayName("if email already used EmailAlreadyUsedException must thrown")
    @Transactional
    void modifyUsed() {
        User target = USERS.get(1);
        Id id = target.getId();
        Email email = USERS.get(2).getEmail();
        Password password = new Password("password");
        Role role = target.getRole();

        assertThrows(EmailAlreadyUsedException.class, () -> service.modify(id, email, password, role));
    }

    @Test
    @DisplayName("if id, email, password and role are invalid ConstraintViolationException must be thrown")
    @Transactional
    void modifyInvalid() {
        for (int h = 0; h < IDS.size(); h++) {
            for (int i = 0; i < EMAILS.size(); i++) {
                for (int j = 0; j < PASSWORDS.size(); j++) {
                    for (int k = 0; k < ROLES.size(); k++) {
                        if (i == 0 && j == 0 && k == 0)
                            continue;
                        Id id = IDS.get(h);
                        Email email = EMAILS.get(i);
                        Password password = PASSWORDS.get(j);
                        Role role = ROLES.get(k);
                        assertThrows(ConstraintViolationException.class, () -> service.modify(id, email, password, role));
                    }
                }
            }
        }
    }

    @Test
    @DisplayName("must be removed")
    @Transactional
    void remove() {
        Id id = USERS.get(1).getId();

        service.remove(id);

        Optional<UserEntity> removing = delegate.findById(id.getValue());
        assertTrue(removing.isEmpty());
    }

    @Test
    @DisplayName("throw UserNotFoundException if user not found")
    @Transactional
    void removeEmpty() {
        Id id = new Id("..................................36");
        assertThrows(UserNotFoundException.class, () -> service.remove(id));
    }

    @Test
    @DisplayName("throw ConstraintViolationException if Id is invalid")
    @Transactional
    void removeInvalid() {
        for (int i = 0; i < IDS.size(); i++) {
            if (i == 0)
                continue;
            Id id = IDS.get(i);
            assertThrows(ConstraintViolationException.class, () -> service.remove(id));
        }
    }

}