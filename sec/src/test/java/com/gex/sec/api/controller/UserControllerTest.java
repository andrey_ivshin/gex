package com.gex.sec.api.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gex.sec.api.mapper.UserResourceMapper;
import com.gex.sec.api.resource.UserResource;
import com.gex.sec.api.security.JwtGenerator;
import com.gex.sec.api.security.Role;
import com.gex.sec.domain.model.User;
import com.gex.sec.persistence.repository.MongoUserRepositoryDelegate;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.Cookie;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.gex.sec.TestUtils.*;
import static com.gex.sec.api.resource.UserResource.*;
import static com.gex.sec.api.security.SecurityUtil.AUTHORIZATION;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    UserResourceMapper mapper;

    @Autowired
    MongoUserRepositoryDelegate delegate;

    @Autowired
    JwtGenerator generator;

    @Test
    @DisplayName("all users must be returned")
    @Transactional
    void getAll() throws Exception {
        MvcResult result = mockMvc
                .perform(get("/api/user")
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN))))
                .andExpect(status().isOk())
                .andReturn();

        List<UserResource> users = JACKSON.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
        });
        assertEquals(
                USERS.stream().map(user -> user.getId().getValue()).collect(Collectors.toList()),
                users.stream().map(UserResource::getId).collect(Collectors.toList()));
    }

    @Test
    @DisplayName("list must be empty if no user found")
    @Transactional
    void getAllEmpty() throws Exception {
        delegate.deleteAll();

        MvcResult result = mockMvc
                .perform(get("/api/user")
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN))))
                .andExpect(status().isOk())
                .andReturn();

        List<UserResource> users = JACKSON.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
        });
        assertTrue(users.isEmpty());
    }

    @Test
    @DisplayName("403 must be returned if caller is USER")
    @Transactional
    void getAllUser() throws Exception {
        mockMvc
                .perform(get("/api/user")
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.USER))))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("401 must be returned if caller is not authorized")
    @Transactional
    void getAllUnauthorized() throws Exception {
        mockMvc
                .perform(get("/api/user"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("user must be returned")
    @Transactional
    void testGet() throws Exception {
        User target = USERS.get(1);

        MvcResult result = mockMvc
                .perform(get("/api/user/" + target.getId().getValue())
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN))))
                .andExpect(status().isOk())
                .andReturn();

        UserResource user = JACKSON.readValue(result.getResponse().getContentAsString(), UserResource.class);
        assertEquals(mapper.map(target), user);
    }

    @Test
    @DisplayName("400 if id is invalid")
    @Transactional
    void testGetInvalid() throws Exception {
        for (int i = 0; i < IDS.size(); i++) {
            if (i == 0 || IDS.get(i) == null)
                continue;
            mockMvc
                    .perform(get("/api/user/" + IDS.get(i))
                            .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN))))
                    .andExpect(status().isBadRequest());
        }
    }

    @Test
    @DisplayName("403 must be returned if caller is USER")
    @Transactional
    void testGetUser() throws Exception {
        mockMvc
                .perform(get("/api/user/0")
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.USER))))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("401 must be returned if caller is not authorized")
    @Transactional
    void testGetUnauthorized() throws Exception {
        mockMvc
                .perform(get("/api/user/0"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("404 must be returned if user not found")
    @Transactional
    void testGetEmpty() throws Exception {
        mockMvc
                .perform(get("/api/user/..................................36")
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN))))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("403 must be returned if caller is authorized")
    @Transactional
    void authorizeAuthorized() throws Exception {
        User target = USERS.get(1);

        mockMvc
                .perform(post("/api/authorize")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JACKSON.writeValueAsString(new AuthorizationRequest(
                                target.getEmail().getValue(), "password")))
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.USER))))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("400 if email or password invalid")
    @Transactional
    void authorizeInvalid() throws Exception {
        for (int i = 0; i < EMAILS.size(); i++) {
            for (int j = 0; j < PASSWORDS.size(); j++) {
                if (i == 0 && j == 0)
                    continue;
                mockMvc
                        .perform(post("/api/authorize")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(JACKSON.writeValueAsString(new AuthorizationRequest(
                                        EMAILS.get(i).getValue(), PASSWORDS.get(j).getValue()))))
                        .andExpect(status().isBadRequest());
            }
        }
    }

    @Test
    @DisplayName("404 if user email not used")
    @Transactional
    void authorizeEmpty() throws Exception {
        User target = USERS.get(1);
        mockMvc
                .perform(post("/api/authorize")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JACKSON.writeValueAsString(new AuthorizationRequest(
                                target.getEmail().getValue() + "1", "password"))))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("403 if user password is wrong")
    @Transactional
    void authorizeWrong() throws Exception {
        User target = USERS.get(1);
        mockMvc
                .perform(post("/api/authorize")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JACKSON.writeValueAsString(new AuthorizationRequest(
                                target.getEmail().getValue(), "password1"))))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("200 if registered")
    @Transactional
    void register() throws Exception {
        String email = "4email@email.com";
        String password = "password";

        MvcResult result = mockMvc
                .perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN)))
                        .content(JACKSON.writeValueAsString(
                                new RegistrationRequest(
                                email, password, password, Role.USER.name()))))
                .andExpect(status().isOk())
                .andReturn();

        UserResource user = JACKSON.readValue(
                result.getResponse().getContentAsString(),
                UserResource.class);
        assertEquals(email, user.getEmail());
    }

    @Test
    @DisplayName("403 if USER")
    @Transactional
    void registerUser() throws Exception {
        String email = "4email@email.com";
        String password = "password";
        mockMvc
                .perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.USER)))
                        .content(JACKSON.writeValueAsString(new RegistrationRequest(
                                email, password, password, Role.USER.name()
                        ))))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("401 if unauthorized")
    @Transactional
    void registerUnauthorized() throws Exception {
        String email = "4email@email.com";
        String password = "password";
        mockMvc
                .perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JACKSON.writeValueAsString(new RegistrationRequest(
                                email, password, password, Role.USER.name()
                        ))))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("400 if email or password or role invalid")
    @Transactional
    void registerInvalid() throws Exception {
        for (int i = 0; i < EMAILS.size(); i++) {
            for (int j = 0; j < PASSWORDS.size(); j++) {
                for (int k = 0; k < ROLES.size(); k++) {
                    if (i == 0 && j == 0 && k == 0)
                        continue;
                    mockMvc
                            .perform(post("/api/user")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN)))
                                    .content(JACKSON.writeValueAsString(new RegistrationRequest(
                                            EMAILS.get(i) == null ? null : EMAILS.get(i).getValue(),
                                            PASSWORDS.get(j) == null ? null : PASSWORDS.get(j).getValue(),
                                            PASSWORDS.get(j) == null ? null : PASSWORDS.get(j).getValue(),
                                            ROLES.get(k) == null ? null : ROLES.get(k).name()
                                    ))))
                            .andExpect(status().isBadRequest());
                }
            }
        }
    }

    @Test
    @DisplayName("400 if email already used")
    @Transactional
    void registerUsed() throws Exception {
        String email = USERS.get(1).getEmail().getValue();
        String password = "password";
        mockMvc
                .perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN)))
                        .content(JACKSON.writeValueAsString(new RegistrationRequest(
                                email, password, password, Role.USER.name()
                        ))))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("200 if modified")
    @Transactional
    void modify() throws Exception {
        User user = USERS.get(1);
        String email = "4email@email.com";
        String password = "password";

        MvcResult result = mockMvc
                .perform(put("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN)))
                        .content(JACKSON.writeValueAsString(new ModificationRequest(
                                user.getId().getValue(),
                                email,
                                password,
                                password,
                                user.getRole().name()
                        ))))
                .andExpect(status().isOk())
                .andReturn();

        UserResource modifiedUser = JACKSON.readValue(result.getResponse().getContentAsString(), UserResource.class);
        assertEquals(email, modifiedUser.getEmail());
    }

    @Test
    @DisplayName("404 if not found")
    @Transactional
    void modifyEmpty() throws Exception {
        User user = USERS.get(1);
        String email = "4email@email.com";
        String password = "password";

        mockMvc
                .perform(put("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN)))
                        .content(JACKSON.writeValueAsString(new ModificationRequest(
                                "..................................36",
                                email,
                                password,
                                password,
                                user.getRole().name()
                        ))))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("403 if USER")
    @Transactional
    void modifyUser() throws Exception {
        User user = USERS.get(1);
        String email = "4email@email.com";
        String password = "password";

        mockMvc
                .perform(put("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.USER)))
                        .content(JACKSON.writeValueAsString(new ModificationRequest(
                                user.getId().getValue(),
                                email,
                                password,
                                password,
                                user.getRole().name()
                        ))))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("401 if unauthorized")
    @Transactional
    void modifyUnauthorized() throws Exception {
        User user = USERS.get(1);
        String email = "4email@email.com";
        String password = "password";

        mockMvc
                .perform(put("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JACKSON.writeValueAsString(new ModificationRequest(
                                user.getId().getValue(),
                                email,
                                password,
                                password,
                                user.getRole().name()
                        ))))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("400 if id or email or password or role invalid")
    @Transactional
    void modifyInvalid() throws Exception {
        for (int h = 0; h < IDS.size(); h++) {
            for (int i = 0; i < EMAILS.size(); i++) {
                for (int j = 0; j < PASSWORDS.size(); j++) {
                    for (int k = 0; k < ROLES.size(); k++) {
                        if (h == 0 && i == 0 && j == 0 && k == 0) {
                            continue;
                        }
                        mockMvc
                                .perform(put("/api/user")
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN)))
                                        .content(JACKSON.writeValueAsString(new ModificationRequest(
                                                IDS.get(h) == null ? null : IDS.get(h).getValue(),
                                                EMAILS.get(i) == null ? null : EMAILS.get(i).getValue(),
                                                PASSWORDS.get(j) == null ? null : PASSWORDS.get(j).getValue(),
                                                PASSWORDS.get(j) == null ? null : PASSWORDS.get(j).getValue(),
                                                ROLES.get(k) == null ? null : ROLES.get(k).name()
                                        ))))
                                .andExpect(status().isBadRequest());
                    }
                }
            }
        }
    }

    @Test
    @DisplayName("400 if email already used")
    @Transactional
    void modifyUsed() throws Exception {
        String email = USERS.get(1).getEmail().getValue();
        String password = "password";
        mockMvc
                .perform(post("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN)))
                        .content(JACKSON.writeValueAsString(new RegistrationRequest(
                                email, password, password, Role.USER.name()
                        ))))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("user must be returned")
    @Transactional
    void remove() throws Exception {
        User target = USERS.get(1);

        mockMvc
                .perform(delete("/api/user/" + target.getId().getValue())
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN))))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("400 if id is invalid")
    @Transactional
    void removeInvalid() throws Exception {
        for (int i = 0; i < IDS.size(); i++) {
            if (i == 0 || IDS.get(i) == null)
                continue;
            mockMvc
                    .perform(delete("/api/user/" + IDS.get(i))
                            .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN))))
                    .andExpect(status().isBadRequest());
        }
    }

    @Test
    @DisplayName("403 must be returned if caller is USER")
    @Transactional
    void removeUser() throws Exception {
        mockMvc
                .perform(delete("/api/user/0")
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.USER))))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("401 must be returned if caller is not authorized")
    @Transactional
    void removeUnauthorized() throws Exception {
        mockMvc
                .perform(delete("/api/user/0"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("404 must be returned if user not found")
    @Transactional
    void removeEmpty() throws Exception {
        mockMvc
                .perform(get("/api/user/..................................36")
                        .cookie(new Cookie(AUTHORIZATION, jwt(Role.ADMIN))))
                .andExpect(status().isNotFound());
    }

}