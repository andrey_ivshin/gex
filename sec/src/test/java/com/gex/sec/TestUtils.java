package com.gex.sec;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gex.sec.api.security.JwtGenerator;
import com.gex.sec.api.security.Role;
import com.gex.sec.api.security.Token;
import com.gex.sec.domain.model.User;
import com.gex.sec.domain.service.IdGenerator;
import com.gex.sec.domain.service.PasswordEncoder;
import com.gex.sec.persistence.mapper.UserEntityMapper;
import com.gex.sec.persistence.repository.MongoUserRepositoryDelegate;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.gex.sec.domain.model.User.*;

@Component
public class TestUtils implements ApplicationContextAware {

    @Autowired
    MongoUserRepositoryDelegate delegate;

    @Autowired
    UserEntityMapper mapper;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    IdGenerator idGenerator;

    @Autowired
    static JwtGenerator jwtGenerator;

    static ApplicationContext context;

    private static boolean EXECUTED = false;

    public static final ObjectMapper JACKSON = new ObjectMapper();

    public static final List<User> USERS = new ArrayList<>();

    public static final List<Id> IDS = List.of(new Id("158cb0b9-65dd-4e1e-a4db-37048ed82363"), new Id(""), new Id(null));

    public static final List<Email> EMAILS = List.of(new Email("email@email.com"),
            new Email(null), new Email("email"));

    public static final List<Password> PASSWORDS = List.of(new Password("password"), new Password(null),
            new Password("......7"), new Password(".................................................51"));

    public static final List<Role> ROLES = Arrays.asList(Role.USER, null);

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    @PostConstruct
    void init() {
        jwtGenerator = context.getBean(JwtGenerator.class);

        if (!EXECUTED) {
            delegate.deleteAll();
            for (int i = 0; i < 3; i++)
                USERS.add(new User(
                        idGenerator.generate(),
                        new Email(i + "email@email.com"),
                        encoder.encode(new Password("password")),
                        Role.USER));
            delegate.insert(USERS.stream().map(mapper::map).collect(Collectors.toList()));
            EXECUTED = true;
        }
    }

    public static String jwt(Role role) {
        return jwtGenerator.generate(new Token("1", role.name()));
    }

}
